# coding: utf-8
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit
from pony.orm.core import QueryResult, desc
from passlib.hash import pbkdf2_sha256
from youpi.commons.exceptions import MissingPassword
from youpi.services import StoreService
from youpi.services.UserService import UserService

class CostumerService:
    @inject
    def __init__(self, db: Database, user_service: UserService, store_service: StoreService):
        self.db = db
        self.user_service = user_service
        self.store_service = store_service

    @db_session
    def identify(self, costumer_id: int):
        return self.db.model.Costumer[costumer_id]

    @db_session
    def add_update(self, email: str, first_name: str, last_name: str, store_id: int=None, extra: dict={},
                   password: str=None, local: str=None, contact: str=None, volume: int=None,
                   total_spent: float=None, id: int=None):

        # if no store provided generate default store
        if store_id is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store_id]

        if id is None:
            if password is None:
                raise MissingPassword

            db_costumer = self.db.model.Costumer(
                email=email,
                password=pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(password),
                first_name=first_name,
                last_name=last_name,
                role=0,
                store=store,
                extra=extra,
                local=local,
                contact=contact,
                volume=volume,
                total_spent=total_spent
            )
            commit()
            self.user_service.send_confirmation_email(db_costumer.id)
        else:
            db_costumer = self.db.model.Costumer[id]
            db_costumer.first_name = first_name
            db_costumer.last_name = last_name
            db_costumer.email = email
            db_costumer.extra = extra
            db_costumer.local = local
            db_costumer.contact = contact
            db_costumer.volume = volume
            db_costumer.total_spent = total_spent

        return self.to_dict(db_costumer)

    @db_session
    def get_costumer_by_id(self, _id):
        costumer = self.db.model.Costumer[_id]
        return self.to_dict(costumer)

    @db_session
    def get_costumers_by_store(self, store, page, page_size, order_by, descendent):
        costumers = self.db.model.Costumer.select(lambda c: c.store == self.db.model.Store[store])

        amount = costumers.count()

        if order_by:
            if descendent:
                costumers = costumers.order_by(desc("c." + order_by))
            else:
                costumers = costumers.order_by("c." + order_by)

        if page:
            costumers = costumers.page(page, page_size)

        response = {
            "count": amount,
            "costumers": [self.to_dict(costumer) for costumer in costumers]
        }

        return response

    @db_session
    def delete_costumer(self, _id):
        costumer = self.db.model.Costumer[_id]
        costumer_json = self.to_dict(costumer)
        costumer.delete()
        return costumer_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Costumer[c.id]) for c in obj]
        else:
            obj = self.db.model.Costumer[obj.id]
            costumer_dict = obj.to_dict()

            costumer_dict["store"] = obj.store.to_dict()

            if costumer_dict["total_spent"] is not None:
                costumer_dict["total_spent"] = f"{costumer_dict['total_spent']}€"

            if isinstance(obj, self.db.model.Associate):
                costumer_dict["contract_exp"] = str(costumer_dict["contract_exp"])

            del costumer_dict["password"]
            del costumer_dict["shopping_cart"]

            return costumer_dict
