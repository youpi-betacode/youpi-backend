from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit, select
from pony.orm.core import QueryResult
from youpi.commons.exceptions import MissingVariantFromCart


class ShoppingCartService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identify(self, cart_id: int):
        return self.db.model.ShoppingCart[cart_id]

    @db_session
    def create_shopping_cart(self, costumer: int):
        db_cart = self.db.model.ShoppingCart(
            costumer=self.db.model.Costumer[costumer],
            total=0,
            amounts={}
        )
        commit()

        return self.to_dict(db_cart)

    @db_session(optimistic=False)
    def add_product_to_cart(self, cart: int, product: str):
        shopping_cart = self.db.model.ShoppingCart[cart]
        product = self.db.model.Product[product]

        if product not in shopping_cart.products:
            shopping_cart.products += product

        if product.reference in shopping_cart.amounts:
            shopping_cart.amounts[product.reference] += 1
        else:
            product_amount = {product.reference: 1}
            shopping_cart.amounts.update(product_amount)

        shopping_cart.total += product.price_base
        return self.to_dict(shopping_cart)

    @db_session
    def add_n_products_to_cart(self, cart: int, product: str, amount: int):
        shopping_cart = self.db.model.ShoppingCart[cart]
        product = self.db.model.Product[product]

        if product not in shopping_cart.products:
            shopping_cart.products += product

        if product.reference in shopping_cart.amounts:
            shopping_cart.amounts[product.reference] += amount
        else:
            product_amount = {product.reference: amount}
            shopping_cart.amounts.update(product_amount)

        shopping_cart.total += (product.price_base * amount)
        return self.to_dict(shopping_cart)

    @db_session
    def remove_product_from_cart(self, cart: int, product: str):
        shopping_cart = self.db.model.ShoppingCart[cart]
        product = self.db.model.Product[product]

        if product not in shopping_cart.products:
            raise MissingVariantFromCart
        else:
            if product.reference in shopping_cart.amounts:
                shopping_cart.amounts[product.reference] -= 1

            if shopping_cart.amounts[product.reference] == 0:
                del shopping_cart.amounts[product.reference]
                shopping_cart.products -= product

            shopping_cart.total -= product.price_base
            return self.to_dict(shopping_cart)

    @db_session
    def remove_n_products_from_cart(self, cart: int, product: str, amount: int):
        shopping_cart = self.db.model.ShoppingCart[cart]
        product = self.db.model.Product[product]

        if product not in shopping_cart.products:
            raise MissingVariantFromCart
        else:
            if product.reference in shopping_cart.amounts:
                shopping_cart.amounts[product.reference] -= amount

            if shopping_cart.amounts[product.reference] == 0:
                del shopping_cart.amounts[product.reference]
                shopping_cart.products -= product

            shopping_cart.total -= (product.price_base * amount)
            return self.to_dict(shopping_cart)

    @db_session
    def get_cart_by_costumer(self, current_user):
        shopping_cart = self.db.model.ShoppingCart.get(costumer=costumer)
        return self.to_dict(shopping_cart)

    @db_session
    def delete_cart(self, _id):
        cart = self.db.model.ShoppingCart[_id]
        cart_json = self.to_dict(cart)
        cart.delete()
        return cart_json

    @db_session(optimistic=False)
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.ShoppingCart[c.id]) for c in obj]
        else:
            obj = self.db.model.ShoppingCart[obj.id]
            cart_dict = obj.to_dict()

            products = {}
            total_amount = 0
            for product in select(o for o in obj.products).order_by(lambda o: o.reference):
                product_dict = product.to_dict()
                product_dict.pop("last_sale")

                amount = obj.amounts[product.reference] if product.reference in obj.amounts else 0
                total_amount += amount
                product_dict["amount"] = amount
                products.update({product.reference: product_dict})

            cart_dict["products"] = products
            cart_dict.pop("amounts")
            cart_dict.update(total_amount=total_amount)

            return cart_dict
