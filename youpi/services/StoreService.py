from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit
from pony.orm.core import QueryResult, ObjectNotFound


class StoreService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identity(self, store_id: int):
        return self.db.model.Store[store_id]

    @db_session
    def add_update(self, name: str, description: str, id: int=None):
        if id is None:
            db_store = self.db.model.Store(
                name=name,
                description=description,
            )
            commit()
        else:
            db_store = self.db.model.Store[id]
            db_store.name = name
            db_store.description = description

        return self.to_dict(db_store)

    @db_session
    def add_default_store(self, format="JSON"):
        try:
            db_store = self.db.model.Store[1]
        except ObjectNotFound:
            db_store = self.db.model.Store(
                name="Default Store",
                description="Default Description"
            )
            commit()

        if format == "JSON":
            return self.to_dict(db_store)
        else:
            return db_store

    @db_session
    def get_store_by_id(self, _id):
        store = self.db.model.Store[_id]
        return self.to_dict(store)

    @db_session
    def get_all_stores(self):
        stores = [self.to_dict(store) for store in self.db.model.Store.select()]
        return stores

    @db_session
    def delete_store(self, _id):
        store = self.db.model.Store[_id]
        store_json = self.to_dict(store)
        store.delete()
        return store_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Store[c.id]) for c in obj]
        else:
            obj = self.db.model.Store[obj.id]
            store_dict = obj.to_dict()

            store_dict["products"] = [product.to_dict() for product in obj.products]

            return store_dict
