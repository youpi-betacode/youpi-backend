import requests
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit
from pony.orm.core import QueryResult
from youpi.commons.exceptions import NoPhotosFound, InvalidVariant, AlreadyAssignedPhoto, UnassignedPhoto,\
    FileManagerUploadError, FileManagerDeleteError


class PhotoService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identify(self, photo_id: int):
        return self.db.model.Photo[photo_id]

    @db_session
    def upload_photo_to_product(self, image, image_name, content_type, token: str, product: int):
        url = "api.filemanager.betacode.tech/v1/files/upload"

        files = {
            "file": (image_name, image, content_type)
        }

        headers = {
            "Authorization": token
        }

        response = requests.post(url, files=files, headers=headers).json()

        if response.status_code != requests.codes.ok:
            raise FileManagerUploadError(response.raise_for_status())

        photo = self.db.model.Photo(
            url="api.filemanager.betacode.tech/v1" + response["url"],
            uuid=response["uuid"],
            product=self.db.model.Product[product]
        )
        commit()
        return self.to_dict(photo)

    @db_session
    def add_photo_to_product(self, url: str, uuid: str, product: int):
        photo = self.db.model.Photo(
            url=url,
            uuid=uuid,
            product=self.db.model.Product[product]
        )
        commit()
        return self.to_dict(photo)

    @db_session
    def add_variant_photo(self, photo, variant):
        p = self.db.model.Photo[photo]
        v = self.db.model.Variant[variant]

        if p.variant:
            raise AlreadyAssignedPhoto

        if p.product == v.product:
            p.variant = v
        else:
            raise InvalidVariant

    @db_session
    def remove_variant_photo(self, photo):
        p = self.db.model.Photo[photo]

        if p.variant:
            p.variant = None
        else:
            raise UnassignedPhoto

    @db_session
    def get_photo_by_id(self, _id):
        photo = self.db.model.Photo[_id]
        return self.to_dict(photo)

    @db_session
    def get_photos_by_product(self, product):
        photos = [self.to_dict(photo) for photo in
                  self.db.model.Photo.select(lambda x: x.product == self.db.model.Product[product])]

        if not photos:
            raise NoPhotosFound
        else:
            return photos

    @db_session
    def get_photos_by_variant(self, variant):
        photos = [self.to_dict(photo) for photo in
                  self.db.model.Photo.select(lambda x: x.variant == self.db.model.Variant[variant])]

        if not photos:
            raise NoPhotosFound
        else:
            return photos

    @db_session
    def delete_photo(self, photo, token):
        chosen_photo = self.db.model.Photo[photo]

        url = f"https://api.filemanager.betacode.tech/v1/files/{chosen_photo.uuid}"

        headers = {
            "Authorization": token
        }

        response = requests.delete(url, headers=headers).json()

        if response.status_code != requests.codes.ok:
            raise FileManagerDeleteError(response.raise_for_status())

        photo_json = self.to_dict(chosen_photo)
        chosen_photo.delete()
        return photo_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Photo[c.id]) for c in obj]
        else:
            obj = self.db.model.Photo[obj.id]
            photo_dict = obj.to_dict()

            return photo_dict
