import datetime
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit, desc
from pony.orm.core import QueryResult
from passlib.hash import pbkdf2_sha256
from youpi.commons.exceptions import InvalidAssociation, MissingPassword


class AssociateService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identify(self, associate_id: int):
        return self.db.model.Associate[associate_id]

    @db_session
    def add_update(self, email: str, first_name: str, last_name: str, store: int, associate_type: str,
                   entity_name: str, contract_exp: datetime.date, extra: dict={}, password: str=None, local: str=None,
                   contact: str=None, volume: int=None, total_spent: float=None, sales_in_store: float=None,
                   sales_online: float=None, id: int=None):

        if associate_type != 'Sponsor' and associate_type != 'Partner':
            raise InvalidAssociation
        if id is None:
            if password is None:
                raise MissingPassword

            db_associate = self.db.model.Associate(
                email=email,
                password=pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(password),
                first_name=first_name,
                last_name=last_name,
                role=1,
                store=self.db.model.Store[store],
                extra=extra,
                associate_type=associate_type,
                entity_name=entity_name,
                contract_exp=contract_exp,
                local=local,
                contact=contact,
                volume=volume,
                total_spent=total_spent,
                sales_in_store=sales_in_store,
                sales_online=sales_online
            )
            commit()
        else:
            db_associate = self.db.model.Associate[id]
            db_associate.first_name = first_name
            db_associate.last_name = last_name
            db_associate.email = email
            db_associate.extra = extra
            db_associate.associate_type = associate_type
            db_associate.entity_name = entity_name
            db_associate.contract_exp = contract_exp
            db_associate.local = local
            db_associate.contact = contact
            db_associate.volume = volume
            db_associate.total_spent = total_spent
            db_associate.sales_in_store = sales_in_store
            db_associate.sales_online = sales_online

        return self.to_dict(db_associate)

    @db_session
    def get_associate_by_id(self, _id):
        associate = self.db.model.Associate[_id]
        return self.to_dict(associate)

    @db_session
    def get_associates_by_store(self, store, page, page_size, order_by, descendent):
        associates = self.db.model.Associate.select(lambda a: a.store == self.db.model.Store[store])

        amount = associates.count()

        if order_by:
            if descendent:
                associates = associates.order_by(desc("a." + order_by))
            else:
                associates = associates.order_by("a." + order_by)

        if page:
            associates = associates.page(page, page_size)

        response = {
            "count": amount,
            "associates": [self.to_dict(associate) for associate in associates]
        }

        return response

    @db_session
    def get_partners_by_store(self, store, page, page_size, order_by, descendent):
        partners = self.db.model.Associate.select(lambda x: x.store == self.db.model.Store[store] and
                                                            x.associate_type == "Partner")

        amount = partners.count()

        if order_by:
            if descendent:
                partners = partners.order_by(desc("a." + order_by))
            else:
                partners = partners.order_by("a." + order_by)

        if page:
            partners = partners.page(page, page_size)

        response = {
            "count": amount,
            "partners": [self.to_dict(partner) for partner in partners]
        }

        return response

    @db_session
    def get_sponsors_by_store(self, store, page, page_size, order_by, descendent):
        sponsors = self.db.model.Associate.select(lambda x: x.store == self.db.model.Store[store] and
                                                            x.associate_type == "Sponsor")

        amount = sponsors.count()

        if order_by:
            if descendent:
                sponsors = sponsors.order_by(desc("a." + order_by))
            else:
                sponsors = sponsors.order_by("a." + order_by)

        if page:
            sponsors = sponsors.page(page, page_size)

        response = {
            "count": amount,
            "sponsors": [self.to_dict(sponsor) for sponsor in sponsors]
        }

        return response

    @db_session
    def delete_associate(self, _id):
        associate = self.db.model.Associate[_id]
        associate_json = self.to_dict(associate)
        associate.delete()
        return associate_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Associate[c.id]) for c in obj]
        else:
            obj = self.db.model.Associate[obj.id]
            associate_dict = obj.to_dict()

            associate_dict["store"] = obj.store.to_dict()
            associate_dict["contract_exp"] = str(associate_dict["contract_exp"])
            del associate_dict["password"]

            return associate_dict
