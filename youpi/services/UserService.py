import uuid
import requests
from flask_injector import inject
from flask import redirect
from youpi.models.entities import Database
from pony.orm import db_session, commit, ObjectNotFound
from pony.orm.core import QueryResult
from youpi.commons.exceptions import InvalidRole, NonMatchingPassword, MissingPassword, EmailFailed
from passlib.hash import pbkdf2_sha256
import os


class UserService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identity(self, user_id: int):
        return self.db.model.User[user_id]

    def add_default_store(self):
        try:
            db_store = self.db.model.Store[1]
        except ObjectNotFound:
            db_store = self.db.model.Store(
                name="Default Store",
                description="Default Description"
            )
        return db_store

    @db_session
    def add_update(self, email: str, first_name: str, last_name: str, role: int, store: int,
                   extra: dict={}, password: str=None, id: int = None):
        if role not in range(2, 5):
            raise InvalidRole
        if id is None:
            if password is None:
                raise MissingPassword

            try:
                store = self.db.model.Store[store]
            except ObjectNotFound:
                store = self.add_default_store()

            db_user = self.db.model.User(
                email=email,
                password=pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(password),
                first_name=first_name,
                last_name=last_name,
                role=role,
                store=store,
                extra=extra
            )
            commit()
        else:
            db_user = self.db.model.User[id]
            db_user.email = email
            db_user.first_name = first_name
            db_user.last_name = last_name
            db_user.role = role
            db_user.extra = extra

        return self.to_dict(db_user)

    @db_session
    def authenticate(self, email: str, password: str):
        user = self.db.model.User.select(lambda x: x.email == email).first()

        if user and pbkdf2_sha256.verify(password, user.password):
            return self.to_dict(user)
        else:
            raise NonMatchingPassword

    @db_session
    def send_confirmation_email(self, _id):
        user = self.db.model.User[_id]
        user.extra.update(email_status=False)
        user.extra.update(uuid=str(uuid.uuid4()))

        EMAIL_ENDPOINT = os.environ.get("EMAIL_ENDPOINT", "http://dev.api.emailhandler.betacode.tech/v1")
        API_ENDPOINT = os.environ.get("API_ENDPOINT", "http://dev.api.youpii.betacode.tech/v1")

        url = f"{EMAIL_ENDPOINT}/email/confirmation"
        body = {
            "email_dest": user.email,
            "confirmation_link": f"{API_ENDPOINT}/email/{user.extra['uuid']}"
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise EmailFailed(response.raise_for_status())

        return self.to_dict(user)

    @db_session
    def confirm_user_email(self, uuid):
        user = self.db.model.User.select(lambda x: x.extra["uuid"] == uuid).first()
        user.extra = {} if user.extra is None else user.extra
        user.extra["email_status"] = True

        STORE_URL = os.environ.get("STORE_URL", "http://dev.store.youpii.pt/user")

        return redirect(STORE_URL, code=302)

    @db_session
    def reset_user_password(self, _id, password):
        user = self.db.model.User[_id]
        user.password = pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(password)

        EMAIL_ENDPOINT = os.environ.get("EMAIL_ENDPOINT", "http://dev.api.emailhandler.betacode.tech/v1")

        url = f"{EMAIL_ENDPOINT}/email/passwd_reset"
        body = {
            "email_dest": user.email
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise EmailFailed(response.raise_for_status())

        return self.to_dict(user)

    @db_session
    def recover_password(self, email):
        user = self.db.model.User.get(email=email)
        new_password = str(uuid.uuid4())[:8]
        user.password = pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(new_password)

        EMAIL_ENDPOINT = os.environ.get("EMAIL_ENDPOINT", "http://dev.api.emailhandler.betacode.tech/v1")

        url = f"{EMAIL_ENDPOINT}/email/passwd_recovery"
        body = {
            "email_dest": user.email,
            "new_passwd": new_password
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise EmailFailed(response.raise_for_status())

        return self.to_dict(user)

    @db_session
    def get_user_by_id(self, _id):
        user = self.db.model.User[_id]
        return self.to_dict(user)

    @db_session
    def get_user_by_email(self, email):
        user = self.db.model.User.get(email=email)
        return self.to_dict(user)

    @db_session
    def get_users_by_store(self, store):
        users = [self.to_dict(user) for user in
                     self.db.model.User.select(lambda u: u.store == self.db.model.Store[store])]
        return users

    @db_session
    def delete_user(self, _id):
        user = self.db.model.User[_id]
        user_json = self.to_dict(user)
        user.delete()
        return user_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.User[c.id]) for c in obj]
        else:
            obj = self.db.model.User[obj.id]
            user_dict = obj.to_dict()
            if isinstance(obj, self.db.model.Associate):
                user_dict["contract_exp"] = str(user_dict["contract_exp"])
            del user_dict["password"]
            del user_dict["shopping_cart"]

            return user_dict
