from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, desc
from youpi.services.StoreService import StoreService
from pony.orm.core import ObjectNotFound
from pony.orm.core import QueryResult


class ProductService:
    @inject
    def __init__(self, db: Database, store_service: StoreService):
        self.db = db
        self.store_service = store_service

    @db_session
    def identity(self, product_id: int):
        product = self.db.model.Product[product_id]
        return product

    @db_session
    def add_update(self, name, description=None, category=None, sub_category=None, reference=None, extra={},
                   store_id=None, price_base=0, price_promo=None, stock=None):

        # if no store provided generate default store
        if store_id is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store_id]

        if reference is None:
            # No reference just add the product
            product = self.db.model.Product(
                name=name,
                description=description,
                category=category,
                sub_category=sub_category,
                extra=extra,
                store=store,
                stock=stock,
                price_promo=price_promo,
                price_base=price_base
            )
        else:
            try:
                # if reference exist update product
                product = self.db.model.Product[reference]

                product.name = name
                product.description = description
                product.category = category
                product.sub_category = sub_category
                product.extra = extra
                product.stock = stock
                product.price_promo = price_promo
                product.price_base = price_base
            except ObjectNotFound:
                # if product does not exist just add product with the provided reference
                product = self.db.model.Product(
                    reference=reference,
                    name=name,
                    description=description,
                    category=category,
                    sub_category=sub_category,
                    extra=extra,
                    store=store,
                    stock=stock,
                    price_promo=price_promo,
                    price_base=price_base
                )

        return self.to_dict(product)

    @db_session
    def get_product_by_id(self, _id):
        product = self.db.model.Product[_id]
        return self.to_dict(product)

    @db_session
    def get_product_parents_by_store(self, store=None, category=None, sub_category=None, supplier=None, page=None,
                              page_size=None, order_by=None, descendent=False):
        if store is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store]

        products = self.db.model.Product.select(lambda p: p.store == store)

        products = products.filter(lambda p: p.parent is None)

        products = products.filter(lambda p: p.extra['inactive'] is False)

        if category:
            products = products.filter(lambda p: p.category == category)

        if sub_category:
            products = products.filter(lambda p: p.sub_category == sub_category)

        amount = products.count()

        if order_by:
            if descendent:
                products = products.order_by(desc("p." + order_by))
            else:
                products = products.order_by("p." + order_by)

        if page:
            products = products.page(page, page_size)

        products = [self.to_dict(product) for product in products]

        response = {
            "count": amount,
            "products": products
        }

        return response

    @db_session
    def get_related_products_parents_by_store(self, store=None, reference=None):
        if store is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store]

        products = self.db.model.Product.select(lambda p: p.store == store)

        product = self.db.model.Product[reference]

        products = products.filter(lambda p: p.parent is None)

        products = products.filter(lambda p: p.reference != product.reference)

        products = products.filter(lambda p: p.category == product.category)

        products = products.filter(lambda p: p.sub_category == product.sub_category)

        products = products.filter(lambda p: p.extra['inactive'] is False)

        amount = products.count()

        products = [self.to_dict(product) for product in products]

        response = {
            "count": amount,
            "products": products
        }

        return response

    @db_session
    def get_products_by_store(self, store=None, category=None, sub_category=None, supplier=None, page=None,
                              page_size=None, order_by=None, descendent=False):
        if store is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store]

        products = self.db.model.Product.select(lambda p: p.store == store)

        if category:
            products = products.filter(lambda p: p.category == category)

        if sub_category:
            products = products.filter(lambda p: p.sub_category == sub_category)

        amount = products.count()

        if order_by:
            if descendent:
                products = products.order_by(desc("p." + order_by))
            else:
                products = products.order_by("p." + order_by)

        if page:
            products = products.page(page, page_size)

        products = [self.to_dict(product) for product in products]

        response = {
            "count": amount,
            "products": products
        }

        return response

    @db_session
    def delete_product(self, _id):
        product = self.db.model.Product[_id]
        product_json = self.to_dict(product)
        product.delete()
        return product_json

    @db_session
    def add_variant(self, id1, id2):
        product1 = self.db.model.Product[id1]
        product2 = self.db.model.Product[id2]

        product2.parent = product1

        if product1.price_base > product2.price_base or product1.price_base == 0:
            product1.price_base = product2.price_base

        return self.to_dict(product1)

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Product[c.reference]) for c in obj]
        else:
            obj = self.db.model.Product[obj.reference]
            product_dict = obj.to_dict()

            product_dict["last_sale"] = str(obj.last_sale)
            product_dict["store"] = obj.store.to_dict()
            product_dict["photos"] = [photo.to_dict() for photo in obj.photos]

            variants = []
            for variant in obj.variants:
                if 'inactive' in variant.extra:
                    if variant.extra['inactive'] is False:
                        variants.append(self.to_dict(variant))

            product_dict["variants"] = sorted(variants, key=lambda v: v['price_base'])

            if obj.supplier is None:
                product_dict["supplier"] = None
            else:
                product_dict["supplier"] = obj.supplier.to_dict()

            return product_dict
