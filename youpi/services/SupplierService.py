from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit
from pony.orm.core import QueryResult, ObjectNotFound


class SupplierService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identify(self, supplier_id: int):
        return self.db.model.Supplier[supplier_id]

    @db_session
    def add_update(self, name: str, description: str, address: str, phone_number: int, email: str, id: int=None):
        if id is None:
            db_supplier = self.db.model.Supplier(
                name=name,
                description=description,
                address=address,
                phone_number=phone_number,
                email=email
            )
            commit()
        else:
            db_supplier = self.db.model.Supplier[id]
            db_supplier.name = name
            db_supplier.description = description
            db_supplier.address = address
            db_supplier.phone_number = phone_number
            db_supplier.email = email

        return self.to_dict(db_supplier)

    @db_session
    def get_supplier_by_id(self, _id):
        supplier = self.db.model.Supplier[_id]
        return self.to_dict(supplier)

    @db_session
    def get_all_suppliers(self):
        suppliers = [self.to_dict(supplier) for supplier in self.db.model.Supplier.select()]
        return suppliers

    @db_session
    def delete_supplier(self, _id):
        supplier = self.db.model.Supplier[_id]
        supplier_json = self.to_dict(supplier)
        supplier.delete()
        return supplier_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Supplier[c.id]) for c in obj]
        else:
            obj = self.db.model.Supplier[obj.id]
            supplier_dict = obj.to_dict()

            supplier_dict["products"] = [product.to_dict() for product in obj.products]

            return supplier_dict
