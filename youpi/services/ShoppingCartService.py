import os
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit, select
from pony.orm.core import QueryResult
from youpi.commons.exceptions import MissingVariantFromCart, ApplicationException
from youpi.services import StoreService
from uuid import UUID
from pony.orm.core import ObjectNotFound

PAID_DELIVERY = os.environ.get("PAID_DELIVERY", 'YSECASA')
FREE_DELIVERY = os.environ.get("FREE_DELIVERY", 'YSECASAGRATUITO')
DELIVERY_PRICE_THRESHOLD = os.environ.get("DELIVERY_PRICE_THRESHOLD", 30)


class ShoppingCartService:
    @inject
    def __init__(self, db: Database, store_service: StoreService):
        self.db = db
        self.store_service = store_service

    @db_session
    def get_costumer_cart(self, costumer_id, store_id=None):
        # if no store provided generate default store
        if store_id is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store_id]

        if costumer_id:
            try:
                costumer = self.db.model.Costumer[costumer_id]
            except ObjectNotFound:
                raise ApplicationException(msg=f"Costumer with id:{costumer_id} does not exists")

            if costumer.shopping_cart is None:
                return self.create_shopping_cart(costumer_id=costumer_id, store_id=store.id)
            else:
                return self.to_dict(costumer.shopping_cart)

    @db_session
    def get_shopping_cart(self, uuid, costumer_id=None):
        try:
            shopping_cart = self.db.model.ShoppingCart[UUID(uuid)]

            if costumer_id is not None and shopping_cart.costumer is None:
                self.add_costumer_to_shopping_cart(costumer_id=costumer_id, uuid=uuid)

            return self.to_dict(shopping_cart)

        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

    @db_session
    def create_shopping_cart(self, costumer_id=None, store_id=None):
        # if no store provided generate default store
        if store_id is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store_id]

        if costumer_id:
            try:
                costumer = self.db.model.Costumer[costumer_id]
            except ObjectNotFound:
                raise ApplicationException(msg=f"Costumer with id:{costumer_id} does not exists")

            shopping_cart = self.db.model.ShoppingCart(costumer=costumer, store=store)
        else:
            shopping_cart = self.db.model.ShoppingCart(store=store)

        return self.to_dict(shopping_cart)

    @db_session
    def add_costumer_to_shopping_cart(self, uuid, costumer_id):
        try:
            costumer = self.db.model.Costumer[costumer_id]
        except ObjectNotFound:
            raise ApplicationException(msg=f"Costumer with id:{costumer_id} does not exists")

        try:
            shopping_cart = self.db.model.ShoppingCart[uuid]
        except ObjectNotFound:
            raise ApplicationException(msg=f"Shopping cart with uuid:{uuid} does not exists")

        shopping_cart.costumer = costumer
        return self.to_dict(shopping_cart)

    @db_session
    def add_update_quantity_product_cart(self, uuid, reference, amount, extra=None):
        shopping_cart_product = self.db.model.ShoppingCartProduct.select(
            lambda p: p.product.reference == reference and p.shopping_cart.uuid == UUID(uuid)).first()

        try:
            shopping_cart = self.db.model.ShoppingCart[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        if shopping_cart_product is None:

            try:
                product = self.db.model.Product[reference]
            except ObjectNotFound:
                raise ApplicationException(f"Product with reference:{reference} does not exists")

            shopping_cart_product = self.db.model.ShoppingCartProduct(
                product=product,
                amount=amount,
                shopping_cart=shopping_cart,
                extra=extra
            )

        else:
            if shopping_cart_product.amount == 0:
                raise ApplicationException(
                    f"Product with reference:{reference} cannot have 0 quantity")
            else:
                shopping_cart_product.amount = amount

            return self.to_dict(shopping_cart)

    @db_session
    def delete_product_cart(self, uuid, reference):
        shopping_cart_product = self.db.model.ShoppingCartProduct.select(
            lambda p: p.product.reference == reference and p.shopping_cart.uuid == UUID(uuid)).first()

        try:
            shopping_cart = self.db.model.ShoppingCart[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        if shopping_cart_product is None:
            raise ApplicationException(
                f"Product with reference:{reference} does not exists in shopping cart with uuid {uuid}")
        else:
            shopping_cart_product.delete()

        cart = self.get_shopping_cart(uuid)

        for p in cart['products']:
            if p['reference'] == FREE_DELIVERY or p['reference'] == PAID_DELIVERY:
                self.delivery_decrement(uuid)

        return self.to_dict(shopping_cart)

    @db_session
    def decrement_product_cart(self, uuid, reference, extra=None):

        shopping_cart_product = self.db.model.ShoppingCartProduct.select(
            lambda p: p.product.reference == reference and p.shopping_cart.uuid == UUID(uuid)).first()

        try:
            shopping_cart = self.db.model.ShoppingCart[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        if shopping_cart_product is None:
            raise ApplicationException(
                f"Product with reference:{reference} does not exists in shopping cart with uuid {uuid}")
        else:
            if shopping_cart_product.amount <= 1:
                shopping_cart_product.delete()
            else:
                shopping_cart_product.amount = shopping_cart_product.amount - 1

        self.delivery_decrement(uuid)

        return self.to_dict(shopping_cart)

    @db_session
    def increment_product_cart(self, uuid, reference, extra=None):

        shopping_cart_product = self.db.model.ShoppingCartProduct.select(
            lambda p: p.product.reference == reference
                      and p.shopping_cart.uuid == UUID(uuid)).first()
        try:
            shopping_cart = self.db.model.ShoppingCart[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        if shopping_cart_product is None or shopping_cart_product and "simulation_data" in shopping_cart_product.extra:
            try:
                product = self.db.model.Product[reference]
            except ObjectNotFound:
                raise ApplicationException(f"Product with reference:{reference} does not exists")

            shopping_cart_product = self.db.model.ShoppingCartProduct(
                product=product,
                amount=1,
                shopping_cart=shopping_cart,
                extra=extra
            )
            self.delivery_increment(uuid)

            return self.to_dict(shopping_cart)
        else:
            shopping_cart_product.amount = shopping_cart_product.amount + 1

        self.delivery_increment(uuid)

        return self.to_dict(shopping_cart)

    @db_session
    def get_shopping_cart_total(self, uuid):
        try:
            cart = self.db.model.ShoppingCart[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        total_price = 0
        for p in cart.products:
            product_dict = p.product.to_dict()
            product_dict["reference"] = p.product.reference
            price = p.product.price_promo
            total_price += p.amount * price

        return round(total_price, 2)

    @db_session
    def to_dict(self, shopping_cart):

        if shopping_cart is None:
            return None
        elif isinstance(shopping_cart, (list, QueryResult)):
            return [self.to_dict(self.db.model.ShoppingCart[c.uuid]) for c in shopping_cart]
        else:
            cart = self.db.model.ShoppingCart[shopping_cart.uuid]
            cart_dict = cart.to_dict()
            cart_dict["uuid"] = str(cart.uuid)

            costumer = cart.costumer
            if costumer is not None:
                cart_dict["costumer"] = {
                    "id": costumer.id,
                    "first_name": costumer.first_name,
                    "last_name": costumer.last_name,
                    "email": costumer.email
                }
            else:
                cart_dict["costumer"] = None

            products = []
            total_products = 0
            total_price = 0
            for p in self.db.model.ShoppingCartProduct.select(lambda s: s.shopping_cart.uuid == cart.uuid).order_by(
                    self.db.model.ShoppingCartProduct.id):
                product_dict = p.product.to_dict()
                product_dict["reference"] = p.product.reference
                product_dict["amount"] = p.amount
                product_dict["extra"] = p.extra
                total_products += p.amount
                price = p.product.price_promo
                total_price += p.amount * price
                products.append(product_dict)

            cart_dict["products"] = products
            cart_dict["total_products"] = total_products
            cart_dict["total_price"] = round(total_price, 2)
            del cart_dict["sale"]
            return cart_dict

    def delivery_increment(self, uuid):
        shopping_cart = self.get_shopping_cart(uuid)

        total = self.get_shopping_cart_total(uuid)
        have_paid_delivery = False
        delevery_price = 0
        delivery_product = None

        for p in shopping_cart['products']:
            if p['reference'] == PAID_DELIVERY:
                have_paid_delivery = True
                delevery_price = p['price_promo']
            if p['reference'] == PAID_DELIVERY or p['reference'] == FREE_DELIVERY:
                delivery_product = p['reference']

        price_threshold = DELIVERY_PRICE_THRESHOLD + delevery_price if have_paid_delivery else DELIVERY_PRICE_THRESHOLD
        if total <= price_threshold:
            if delivery_product == FREE_DELIVERY:
                self.delete_product_cart(uuid, FREE_DELIVERY)
            if delivery_product == PAID_DELIVERY:
                self.delete_product_cart(uuid, PAID_DELIVERY)
            self.add_update_quantity_product_cart(uuid=uuid, reference=PAID_DELIVERY, amount=1)
        else:
            if delivery_product == PAID_DELIVERY:
                self.delete_product_cart(uuid, PAID_DELIVERY)
            if delivery_product == FREE_DELIVERY:
                self.delete_product_cart(uuid, FREE_DELIVERY)

            self.add_update_quantity_product_cart(uuid=uuid, reference=FREE_DELIVERY, amount=1)

    def delivery_decrement(self, uuid):
        shopping_cart = self.get_shopping_cart(uuid)

        total = self.get_shopping_cart_total(uuid)
        have_paid_delivery = False
        delevery_price = 0
        delivery_product = None

        for p in shopping_cart['products']:
            if p['reference'] == PAID_DELIVERY:
                have_paid_delivery = True
                delevery_price = p['price_promo']

            if p['reference'] == PAID_DELIVERY or p['reference'] == FREE_DELIVERY:
                delivery_product = p['reference']

        price_threshold = DELIVERY_PRICE_THRESHOLD + delevery_price if have_paid_delivery else DELIVERY_PRICE_THRESHOLD
        if len(shopping_cart['products']) == 1:
            self.delete_product_cart(uuid, delivery_product)
        else:
            if total <= price_threshold:
                if delivery_product == FREE_DELIVERY:
                    self.delete_product_cart(uuid, FREE_DELIVERY)
                self.add_update_quantity_product_cart(uuid=uuid, reference=PAID_DELIVERY, amount=1)
            else:
                if delivery_product == PAID_DELIVERY:
                    self.delete_product_cart(uuid, PAID_DELIVERY)
                self.add_update_quantity_product_cart(uuid=uuid, reference=FREE_DELIVERY, amount=1)
