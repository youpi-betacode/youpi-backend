from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, commit, desc
from pony.orm.core import QueryResult, CacheIndexError
from youpi.commons.exceptions import RepeatedContacts, EmailRequired


class ContactService:
    @inject
    def __init__(self, db: Database):
        self.db = db

    @db_session
    def identity(self, contact_id: int):
        return self.db.model.Contact[contact_id]

    @db_session
    def add_contact(self, first_name: str, last_name: str, email: str, phone_number: str, address: str,
                    birth_date: str, segment: str, status: str):
        contact = self.db.model.Contact(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone_number=phone_number,
            address=address,
            birth_date=birth_date,
            segment=segment,
            status=status
        )
        commit()

        return self.to_dict(contact)

    @db_session
    def add_contacts(self, contacts):
        self.db.model.Contact.select().delete()
        index = 2
        try:
            for contact in contacts:
                keys = list(contact.keys())
                self.db.model.Contact(
                    first_name=contact[keys[0]],
                    last_name=contact[keys[1]],
                    email=contact[keys[2]],
                    phone_number=contact[keys[3]],
                    address=contact[keys[4]],
                    birth_date=contact[keys[5]],
                    segment=contact[keys[6]],
                    company=contact[keys[7]],
                    business_area=contact[keys[8]],
                    local=contact[keys[9]],
                    status=contact[keys[10]]
                )
                commit()
                index += 1

            return contacts

        except CacheIndexError:
            raise RepeatedContacts(index)

        except ValueError:
            raise EmailRequired(index)

    @db_session
    def get_all_contacts(self, page, page_size, order_by, descendent):
        contacts = self.db.model.Contact.select()

        amount = contacts.count()

        if order_by:
            if descendent:
                contacts = contacts.order_by(desc("c." + order_by))
            else:
                contacts = contacts.order_by("c." + order_by)

        if page:
            contacts = contacts.page(page, page_size)

        response = {
            "count": amount,
            "contacts": [self.to_dict(contact) for contact in contacts]
        }

        return response

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Contact[c.id]) for c in obj]
        else:
            obj = self.db.model.Contact[obj.id]
            contact_dict = obj.to_dict()

            return contact_dict
