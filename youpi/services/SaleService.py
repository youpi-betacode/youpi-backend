# coding: latin-1
import os
import uuid
import requests
import logging
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, desc
from pony.orm.core import QueryResult
from youpi.commons.exceptions import PaymentRequestFailed, ApplicationException, FileManagerUploadError
from youpi.services import ShoppingCartService, StoreService
from pony.orm.core import ObjectNotFound
from uuid import UUID

EMAIL_SERVICE_URL = os.environ.get("EMAIL_SERVICE_URL", "http://api.emailhandler.betacode.tech/v1")
DELIVERY_PRICE_THRESHOLD = os.environ.get("DELIVERY_PRICE_THRESHOLD", 30)
PAYMENT_ENDPOINT = os.environ.get("PAYMENT_ENDPOINT", "http://dev.api.paymentgateways.betacode.tech/v1")
CALLBACK_EMAIL = os.environ.get("CALLBACK_EMAIL", "tiago.marques@betacode.tech")
URL_CALLBACK = os.environ.get("URL_CALLBACK", "http://dev.api.youpii.betacode.tech/v1/hipay/callback/")
URL_ACCEPT = os.environ.get("URL_ACCEPT", "http://dev.store.youpii.pt/checkout/status/")
URL_CANCEL = os.environ.get("URL_CANCEL", "http://dev.store.youpii.pt/")
URL_DECLINE = os.environ.get("URL_DECLINE", "http://dev.store.youpii.pt/")
URL_LOGO = os.environ.get("URL_LOGO", "https://trello-attachments.s3.amazonaws.com/5d0795d0a145ea1c06ca85d9"
                                      "/5de531ab92628a67ad00ec05/ec10afc35da3673e09c4f9d70596d57b/youpi_logo.png")


class SaleService:
    PAYMENT_TYPE_MULTIBANCO = "multibanco"
    PAYMENT_TYPE_PAYSHOP = "payshop"
    PAYMENT_TYPE_CREDIT = ["mastercard", "visa", "maestro"]

    @inject
    def __init__(self, db: Database, shopping_cart_service: ShoppingCartService, store_service: StoreService):
        self.db = db
        self.shopping_cart_service = shopping_cart_service
        self.store_service = store_service

    @db_session
    def get_sale_by_uuid(self, uuid: str):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with id:{uuid} does not exist.")
        return self.to_dict(sale)

    @db_session
    def create_sale(self, costumer: int, payment: str, shopping_cart_uuid: str, extra: dict = {}, store_id=None):

        # if no store provided generate default store
        if store_id is None:
            store = self.store_service.add_default_store(format="OBJECT")
        else:
            store = self.db.model.Store[store_id]

        try:
            costumer = self.db.model.Costumer[costumer]
        except ObjectNotFound:
            raise ApplicationException(f"Costumer with id:{costumer} does not exist.")

        try:
            shopping_cart = self.db.model.ShoppingCart[shopping_cart_uuid]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exist.")

        total = self.shopping_cart_service.get_shopping_cart_total(shopping_cart_uuid)

        sale = self.db.model.Sale(
            total=total,
            payment_type=payment,
            costumer=costumer,
            cart=shopping_cart,
            store=store,
            extra=extra
        )

        payment_response = None
        if payment == SaleService.PAYMENT_TYPE_MULTIBANCO:
            payment_response = self.request_payment_mb(email=costumer.email, amount=total)
        elif payment == SaleService.PAYMENT_TYPE_PAYSHOP:
            payment_response = self.request_payment_payshop(email=costumer.email, amount=total)
        elif payment in SaleService.PAYMENT_TYPE_CREDIT:
            payment_response = self.request_payment_credit(email=costumer.email, amount=total, sale_uuid=str(sale.uuid))

        extra["payment_info"] = payment_response
        sale.extra = extra

        return self.to_dict(sale)

    @staticmethod
    def request_payment_mb(email: str, amount: float):
        url = f"{PAYMENT_ENDPOINT}/hipay/mb_reference"
        body = {
            "email": email,
            "amount": amount
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentRequestFailed(response.raise_for_status())
        else:
            response_json = response.json()
            if "error" in response_json and response_json["error"]:
                raise ApplicationException(msg=response_json["error"])
            return response_json

    @staticmethod
    def request_payment_payshop(email: str, amount: float):
        url = f"{PAYMENT_ENDPOINT}/hipay/payshop_reference"
        body = {
            "email": email,
            "amount": amount
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentRequestFailed(response.raise_for_status())
        else:
            response_json = response.json()
            if "error" in response_json and response_json["error"]:
                raise ApplicationException(msg=response_json["error"])
            return response_json

    @staticmethod
    def request_payment_credit(email: str, amount: float, sale_uuid: str):
        url = f"{PAYMENT_ENDPOINT}/hipay/credit_payment_page"
        body = {
            "callback_email": CALLBACK_EMAIL,
            "url_callback": URL_CALLBACK + sale_uuid if URL_CALLBACK != "" else "",
            "url_accept": URL_ACCEPT + sale_uuid if URL_ACCEPT != "" else "",
            "url_decline": URL_DECLINE,
            "url_cancel": URL_CANCEL,
            "url_logo": URL_LOGO,
            "customer_email": email,
            "amount": amount,
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentRequestFailed(response.raise_for_status())
        else:
            response_json = response.json()
            if "error" in response_json and response_json["error"]:
                raise ApplicationException(msg=response_json["error"])
            return response_json

    @staticmethod
    def process_products(products):
        products_info = []

        for p in products:
            product_name = p['name']
            product_price = p['price_base']
            product_ref = p['reference']
            product_info = {
                "product_name": product_name,
                "product_price": product_price,
                "product_ref": product_ref

            }
            if p['extra'] is not None:
                simulation_data = p['extra']['simulation_data'] if 'simulation_data' in p['extra'] else None
                if simulation_data is not None:
                    frame_size = simulation_data['size'][0]
                    frame_type = simulation_data['gama']
                    white_margin = simulation_data['have_white_margin']
                    picture_size = simulation_data['size'][1]

                    print_picture = ""
                    product_info['frame_size'] = frame_size
                    product_info['frame_type'] = frame_type
                    product_info['white_margin'] = white_margin
                    product_info['picture_size'] = picture_size
                    product_info['print_picture'] = print_picture

            products_info.append(product_info)

        return products_info

    @staticmethod
    def send_purchase_summary(sale, costumer, products):
        addresses = sale['extra']['addresses']

        data = {
            "email_dest": costumer['email'],
            "client_name": f'{costumer["first_name"]} {costumer["last_name"]}',
            "client_email": costumer['email'],
            "client_address": f'{addresses["billing_address"]}, {addresses["billing_city"]}, {addresses["billing_zip_code"]}',
            "client_phone": addresses['shipping_phone_number'],
            "sale_uid": sale['uuid'],
            "sale_date": sale['timestamp'],
            "sale_total": sale['total'],
            "products": products,
            "payment_method": sale['payment_type']
        }

        if 'billing_nif' in addresses:
            data["client_nif"] = addresses['billing_nif']
        else:
            data["client_nif"] = ""


        payment_info = sale['extra']['payment_info']

        if sale['payment_type'] == 'multibanco':
            data['payment_mb_reference'] = payment_info['reference']
            data['payment_mb_entity'] = payment_info['entity']
        if sale['payment_type'] == 'payshop':
            data['payment_ps_reference'] = payment_info['reference']
            data['payment_ps_limit_date'] = payment_info['paymentDeadline']

        url = f'{EMAIL_SERVICE_URL}/email/purchase_client_summary'

        response = requests.post(url, json=data)

        if response.status_code != requests.codes.ok:
            raise FileManagerUploadError(response.raise_for_status())

        logging.info(f'Email sent to {costumer["email"]}')

    @db_session
    def get_sales_to_process(self):
        sales = self.db.model.Sale.select().filter(lambda s: s.status == Database.SALE_STATUS_PAID)
        return [self.to_dict(sale) for sale in sales]

    @db_session
    def get_pending_sales(self):
        sales = self.db.model.Sale.select().filter(lambda s: s.status == Database.SALE_STATUS_PENDING)
        return [self.to_dict(sale) for sale in sales]

    @db_session
    def update_payment_info(self, uuid: str, payment_info: dict):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with uuid: {uuid} does not exist.")

        if "payment_info" in sale.extra:
            sale.extra["payment_info"] = payment_info

        return self.to_dict(sale)

    @db_session
    def change_sale_status_to_paid(self, uuid: str):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with uuid: {uuid} does not exist.")

        sale.status = Database.SALE_STATUS_PAID
        return self.to_dict(sale)

    @db_session
    def change_sale_status_to_processed(self, uuid: str):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with uuid: {uuid} does not exist")

        sale.status = Database.SALE_STATUS_PROCESSED
        return self.to_dict(sale)

    @db_session
    def change_sale_status_to_failed(self, uuid: str):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with uuid: {uuid} does not exist.")

        sale.status = Database.SALE_STATUS_FAILED
        return self.to_dict(sale)

    @db_session
    def delete_sale(self, uuid: str):
        try:
            sale = self.db.model.Sale[UUID(uuid)]
        except ObjectNotFound:
            raise ApplicationException(f"Sale with uuid:{uuid} does not exist.")
        sale_json = self.to_dict(sale)
        sale.delete()
        return sale_json

    @db_session
    def get_sales_by_costumer(self, costumer, page, page_size):
        sales = self.db.model.Sale.select(lambda s: s.costumer == self.db.model.Costumer[costumer])

        amount = sales.count()

        if page and page_size:
            sales = sales.page(page, page_size)

        response = {
            "count": amount,
            "sales": [self.to_dict(sale) for sale in sales]
        }

        return response

    @db_session
    def get_sales_by_store(self, store, page, page_size, order_by, descendent):
        sales = self.db.model.Sale.select(lambda s: s.costumer.store == self.db.model.Store[store])

        amount = sales.count()

        if order_by:
            if descendent:
                sales = sales.order_by(desc("s." + order_by))
            else:
                sales = sales.order_by("s." + order_by)

        if page and page_size:
            sales = sales.page(page, page_size)

        response = {
            "count": amount,
            "sales": [self.to_dict(sale) for sale in sales]
        }

        return response

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Sale[c.uuid]) for c in obj]
        else:
            sale = self.db.model.Sale[obj.uuid]
            sale_dict = sale.to_dict()

            sale_dict["uuid"] = str(sale.uuid)
            sale_dict["timestamp"] = str(sale_dict["timestamp"])
            sale_dict["cart"] = self.shopping_cart_service.to_dict(sale.cart)

            return sale_dict
