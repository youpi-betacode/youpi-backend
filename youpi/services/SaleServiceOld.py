# coding: latin-1
import os
import uuid
import requests
from datetime import datetime, timedelta, date
from flask_injector import inject
from youpi.models.entities import Database
from pony.orm import db_session, desc
from pony.orm.core import QueryResult, count, sum, select
from youpi.commons.exceptions import EmptyShoppingCart, PaymentRequestFailed, ApplicationException
from youpi.services import ShoppingCartService
from pony.orm.core import ObjectNotFound

class SaleService:
    @inject
    def __init__(self, db: Database, shopping_cart_service: ShoppingCartService):
        self.db = db
        self.shopping_cart_service = shopping_cart_service

    @db_session
    def identify(self, sale_id: int):
        return self.db.model.Sale[sale_id]

    @db_session
    def add_sale(self, costumer: int, payment: str, shopping_cart_uuid: str, extra: dict={}):

        try:
            costumer = self.db.model.Costumer[costumer]
        except ObjectNotFound:
            raise ApplicationException(f"Costumer with id:{costumer} does not exists")

        try:
            shopping_cart = self.db.model.ShoppingCart[shopping_cart_uuid]
        except ObjectNotFound:
            raise ApplicationException(f"Shopping cart with uuid:{uuid} does not exists")

        total = self.shopping_cart_service.get_shopping_cart_total(shopping_cart_uuid)

        payment_response = None

        if payment == 'multibanco':
            payment_response = self.request_payment_mb(email=costumer.email, amount=total)
        elif payment == 'payshop':
            payment_response = self.request_payment_payshop(email=costumer.email, amount=total)

        sale = self.db.model.Sale(
            total=total,
            payment_type=payment,
            sale_date=datetime.now(),
            costumer=costumer,
            cart=shopping_cart,
            extra={**extra, **{"payment_info": payment_response}}
        )
        return self.to_dict(sale)

    @staticmethod
    def request_payment_mb(email: str, amount: float):
        PAYMENT_ENDPOINT = os.environ.get("PAYMENT_ENDPOINT", "http://dev.api.paymentgateways.betacode.tech/v1")

        url = f"{PAYMENT_ENDPOINT}/hipay/mb_reference"
        body = {
            "email": email,
            "amount": amount
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentRequestFailed(response.raise_for_status())
        else:
            return response.json()

    @staticmethod
    def request_payment_payshop(email: str, amount: float):
        PAYMENT_ENDPOINT = os.environ.get("PAYMENT_ENDPOINT", "http://dev.api.paymentgateways.betacode.tech/v1")

        url = f"{PAYMENT_ENDPOINT}/hipay/payshop_reference"
        body = {
            "email": email,
            "amount": amount
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentRequestFailed(response.raise_for_status())
        else:
            return response.json()

    @db_session
    def set_sale_status_to_paid(self, uuid: str):
        sale = self.db.model.Sale[uuid]
        sale.status = self.db.model.Sale.PAID

        return self.to_dict(sale)

    @db_session
    def get_sale_by_uuid(self, uuid: str):
        sale = self.db.model.Sale[uuid]
        return self.to_dict(sale)

    @db_session
    def get_sales_to_process(self):
        sales = self.db.model.Sale.select().filter(lambda s: s.status == self.db.model.Sale.PAID)
        return [self.to_dict(sale) for sale in sales]

    @db_session
    def get_pending_sales(self):
        sales = self.db.model.Sale.select().filter(lambda s: s.status == self.db.model.Sale.PENDING)
        return [self.to_dict(sale) for sale in sales]

    # @db_session
    # def get_sales_by_costumer(self, costumer, page, page_size, order_by, descendent):
    #     sales = self.db.model.Sale.select(lambda s: s.costumer == self.db.model.Costumer[costumer]).first()
    #
    #     amount = sales.count()
    #
    #     if order_by:
    #         if descendent:
    #             sales = sales.order_by(desc("s." + order_by))
    #         else:
    #             sales = sales.order_by("s." + order_by)
    #
    #     if page and page_size:
    #         sales = sales.page(page, page_size)
    #
    #     response = {
    #         "count": amount,
    #         "sales": [self.to_dict(sale) for sale in sales]
    #     }
    #
    #     return response
    @db_session
    def get_sales_by_store(self, store, page, page_size, order_by, descendent):
        sales = self.db.model.Sale.select(lambda s: s.costumer.store == self.db.model.Store[store])

        amount = sales.count()

        if order_by:
            if descendent:
                sales = sales.order_by(desc("s." + order_by))
            else:
                sales = sales.order_by("s." + order_by)

        if page and page_size:
            sales = sales.page(page, page_size)

        response = {
            "count": amount,
            "sales": [self.to_dict(sale) for sale in sales]
        }

        return response

    @db_session
    def get_sales_by_product(self, product):
        sales_volume = select((count(str(v.id) + "" + str(s.id)), sum(v.price_base), count(s.costumer))
                              for s in self.db.model.Sale
                              for v in s.variants
                              if v.product == self.db.model.Product[product]).first()

        response = {
            "volume": sales_volume[0],
            "total": round(sales_volume[1], 0),
            'buyers': sales_volume[2]
        }

        return response

    @db_session
    def get_channel_sales(self, store):
        response = {}
        channels = [
            "Web",
            "App",
            "Facebook",
            "Instagram",
            "Store",
            "Partner",
            "Sponsor"
        ]

        for channel in channels:
            values = select((count(s), sum(s.total)) for s in self.db.model.Sale
                            if s.costumer.store == self.db.model.Store[store] and s.channel == channel).first()

            if values[0] and values[1]:
                response[channel] = {
                    "volume": values[0],
                    "total": round(values[1], 2)
                }

        return response

    @db_session
    def get_categories_sales(self, store, timespan):
        response = {}
        num_sales = 0
        categories = [
            "YOUPIs",
            "T�xteis",
            "Frag�ncias",
            "Papel de Parede"
        ]
        date_span = datetime.now() - timedelta(days=timespan)

        for category in categories:
            total_sold = count(str(v.id) + "" + str(s.id) for s in self.db.model.Sale for v in s.variants
                               if v.product.category == category and
                               v.product.store == self.db.model.Store[store] and
                               date_span <= s.sale_date and
                               s.sale_date <= datetime.now())

            num_sales += total_sold
            if total_sold:
                response[category] = {
                    "total_sold": total_sold
                }

        response["Total"] = num_sales
        return response

    @db_session
    def get_products_sales(self, store, timespan):
        response = {}

        today = date.today()
        index = (today.weekday() + 1) % 7
        end_date = today - timedelta(days=index)
        start_date = end_date - timedelta(days=7)

        products = self.db.model.Product.select(lambda p: p.store == self.db.model.Store[store])

        for week in range(timespan):
            products_dict = {}
            for product in products:
                sales_volume = count(str(v.id) + "" + str(s.id) for s in self.db.model.Sale for v in s.variants
                                      if v.product == self.db.model.Product[product.id] and
                                      start_date <= s.sale_date and
                                      s.sale_date <= end_date)

                products_dict[product.name] = sales_volume

            response[str(end_date)] = products_dict

            end_date = start_date
            start_date = end_date - timedelta(days=7)

        return response

    @db_session
    def get_sponsor_sales(self, store):
        response = {}

        sponsors = self.db.model.Associate.select(lambda a: a.associate_type == 'Sponsor')
        products = self.db.model.Product.select(lambda p: p.store == self.db.model.Store[store])

        for sponsor in sponsors:
            products_dict = {}
            for product in products:
                sales_volume = select((count(str(v.id) + "" + str(s.id)), sum(v.price_base)) for s in self.db.model.Sale
                                      for v in s.variants
                                      if v.product == self.db.model.Product[product.id] and
                                      s.costumer == self.db.model.Associate[sponsor.id]).first()

                if sales_volume[0] and sales_volume[1]:
                    products_dict[product.name] = {
                        "volume": sales_volume[0],
                        "total": round(sales_volume[1], 2)
                    }

            response[sponsor.entity_name] = products_dict

        return response

    @db_session
    def delete_sale(self, _id):
        sale = self.db.model.Sale[_id]
        sale_json = self.to_dict(sale)
        sale.delete()
        return sale_json

    @db_session
    def to_dict(self, obj):
        if obj is None:
            return None
        elif isinstance(obj, (list, QueryResult)):
            return [self.to_dict(self.db.model.Sale[c.uuid]) for c in obj]
        else:
            sale = self.db.model.Sale[obj.uuid]
            sale_dict = sale.to_dict()

            sale_dict["timestamp"] = str(sale_dict["timestamp"])
            sale_dict["cart"] = self.shopping_cart_service.to_dict(sale.cart)

            return sale_dict
