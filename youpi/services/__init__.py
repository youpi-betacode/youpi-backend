from .UserService import UserService
from .VersionService import VersionService
from .ProductService import ProductService
from .StoreService import StoreService
from .SupplierService import SupplierService
from .CostumerService import CostumerService
from .ShoppingCartService import ShoppingCartService
from .ContactService import ContactService
from .AssociateService import AssociateService
from .SaleService import SaleService
from .PhotoService import PhotoService

SERVICES = [
    UserService,
    VersionService,
    ProductService,
    StoreService,
    SupplierService,
    ContactService,
    ShoppingCartService,
    CostumerService,
    AssociateService,
    SaleService,
    PhotoService,
]

