import datetime
from pony.orm import *
from pony.orm import Database as PonyDatabase
import uuid


class Database:
    SALE_STATUS_PENDING = 'SALE_STATUS_PENDING'
    SALE_STATUS_PAID = 'SALE_STATUS_PAID'
    SALE_STATUS_SHIPPED = 'SALE_STATUS_SHIPPED'
    SALE_STATUS_PROCESSED = 'SALE_STATUS_PROCESSED'
    SALE_STATUS_DELIVERED = 'SALE_STATUS_DELIVERED'
    SALE_STATUS_FAILED = 'SALE_STATUS_FAILED'

    def __init__(self, **db_params):
        self.model = PonyDatabase(**db_params)

        class User(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            email = Required(str, unique=True)
            password = Required(str)
            first_name = Required(str)
            last_name = Required(str)
            role = Required(int, default=0)
            extra = Optional(Json, default={})
            store = Required("Store")

        class Costumer(User):
            local = Optional(str, nullable=True)
            contact = Optional(str, nullable=True)
            volume = Optional(int)
            total_spent = Optional(float, default=0)
            sales = Set("Sale")
            shopping_cart = Optional("ShoppingCart", cascade_delete=True)

        class Associate(Costumer):
            associate_type = Required(str)
            entity_name = Required(str)
            contract_exp = Required(datetime.date)
            sales_in_store = Optional(float, default=0)
            sales_online = Optional(float, default=0)

        class Store(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            name = Required(str, unique=True)
            description = Optional(str, nullable=True)
            extra = Optional(Json)
            products = Set("Product", cascade_delete=True)
            users = Set("User", cascade_delete=True)
            shopping_carts = Set("ShoppingCart", cascade_delete=True)
            sales = Set("Sale", cascade_delete=True)

        class ShoppingCart(self.model.Entity):
            uuid = PrimaryKey(uuid.UUID, default=uuid.uuid4)
            products = Set("ShoppingCartProduct")
            costumer = Optional("Costumer")
            sale = Optional("Sale")
            store = Required("Store")

        class Sale(self.model.Entity):

            uuid = PrimaryKey(uuid.UUID, default=uuid.uuid4)
            status = Required(str, default=Database.SALE_STATUS_PENDING)
            total = Required(float)
            payment_type = Required(str)
            timestamp = Required(datetime.date, default=datetime.datetime.now())
            extra = Optional(Json)
            cart = Required("ShoppingCart")
            costumer = Required("Costumer")
            store = Required("Store")

        class ShoppingCartProduct(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            product = Required("Product")
            amount = Required(int, default=0)
            extra = Optional(Json, default=None, nullable=True)
            shopping_cart = Required("ShoppingCart")

        class Product(self.model.Entity):
            reference = PrimaryKey(str, default=str(uuid.uuid4()))
            name = Required(str)
            description = Optional(str, nullable=True)
            category = Optional(str, nullable=True)
            sub_category = Optional(str, nullable=True)
            last_sale = Optional(datetime.date, nullable=True)
            price_base = Required(float, default=0)
            price_promo = Optional(float, default=None, nullable=True)
            stock = Optional(int, default=None, nullable=True)
            extra = Optional(Json, nullable=True)
            store = Required("Store")
            parent = Optional("Product")
            variants = Set("Product", cascade_delete=True)
            photos = Set("Photo", cascade_delete=True)
            supplier = Optional("Supplier")
            shopping_cart_product = Set("ShoppingCartProduct")

        class Supplier(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            name = Required(str, unique=True)
            description = Required(str, nullable=True)
            address = Required(str, nullable=True)
            phone_number = Required(int, nullable=True)
            email = Required(str, unique=True)
            extra = Optional(Json)
            products = Set("Product", cascade_delete=True)

        class Photo(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            url = Required(str)
            uuid = Required(uuid.UUID, unique=True, default=uuid.uuid4)
            product = Required("Product")

        class Contact(self.model.Entity):
            id = PrimaryKey(int, auto=True)
            first_name = Optional(str, nullable=True)
            last_name = Optional(str, nullable=True)
            email = Required(str, unique=True)
            phone_number = Optional(int, nullable=True)
            address = Optional(str, nullable=True)
            birth_date = Optional(str, nullable=True)
            segment = Optional(str, nullable=True)
            company = Optional(str, nullable=True)
            business_area = Optional(str, nullable=True)
            local = Optional(str, nullable=True)
            status = Optional(str, nullable=True)
