import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.SaleService import SaleService
from youpi.services.StoreService import StoreService
from youpi.integration.DriveFX import DriveFXService
from youpi.services.ProductService import ProductService
from youpi.services.CostumerService import CostumerService
from youpi.services.ShoppingCartService import ShoppingCartService
from pony.orm.core import ObjectNotFound, ExprEvalError
from youpi.commons.exceptions import EmptyShoppingCart


class SaleServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

        store_service = self.injector.get(StoreService)
        drivefx_service = self.injector.get(DriveFXService)

        # Add default store
        store_service.add_default_store()
        # sync products with drivefx on startup
        drivefx_service.sync_products()

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        sale_service = self.injector.get(SaleService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Sale)
        self.assertIsNotNone(sale_service)

    def testCreateSale(self):
        sale_service = self.injector.get(SaleService)
        cart_service = self.injector.get(ShoppingCartService)
        self.assertIsNotNone(sale_service)

        total = 2123.223
        channel = "Instagram"
        status = "Processing"
        costumer = 3

        cart = cart_service.get_cart_by_costumer(costumer)
        cart = cart_service.add_variant_to_cart(cart["id"], 1)
        sale = sale_service.add_sale(total, channel, status, costumer)
        self.assertIsNotNone(sale)
        self.assertEqual(sale["total"], total)
        self.assertIsNotNone(sale["sale_date"])
        self.assertIsNotNone(sale["sale_time"])
        self.assertEqual(sale["channel"], channel)
        self.assertEqual(sale["status"], status)
        self.assertIsNotNone(sale["costumer"])

    def testCreateSaleWithEmptyCart(self):
        sale_service = self.injector.get(SaleService)
        self.assertIsNotNone(sale_service)

        total = 2123.223
        channel = "Instagram"
        status = "Processing"
        costumer = 3

        with self.assertRaises(EmptyShoppingCart):
            sale_service.add_sale(total, channel, status, costumer)

    def testDeleteSale(self):
        sale_service = self.injector.get(SaleService)
        self.assertIsNotNone(sale_service)

        sale = sale_service.get_sale_by_id(1)
        self.assertIsNotNone(sale)

        deleted_sale = sale_service.delete_sale(sale["id"])
        self.assertIsNotNone(deleted_sale)

        with self.assertRaises(ObjectNotFound):
            sale_service.get_sale_by_id(deleted_sale["id"])

    def testDeleteNonExistingSale(self):
        sale_service = self.injector.get(SaleService)
        self.assertIsNotNone(sale_service)

        with self.assertRaises(ObjectNotFound):
            sale_service.delete_sale(1000)

    def testGetSalesByStore(self):
        sale_service = self.injector.get(SaleService)
        self.assertIsNotNone(sale_service)

        sales = sale_service.get_sales_by_store(1, 1, 5, "id", False)
        self.assertIsNotNone(sales)

    def testGetSalesFromNonExistingStore(self):
        sale_service = self.injector.get(SaleService)
        self.assertIsNotNone(sale_service)

        with self.assertRaises(ExprEvalError):
            sale_service.get_sales_by_store(1000, 1, 5, "id", False)

    def testGetSalesToProcess(self):
        sale_service = self.injector.get(SaleService)
        costumer_service = self.injector.get(CostumerService)
        shopping_cart_service = self.injector.get(ShoppingCartService)
        product_service = self.injector.get(ProductService)
        self.assertIsNotNone(sale_service)
        self.assertIsNotNone(costumer_service)

        password = "password123"
        email = "email@email.email"
        store_id = 1
        first_name = "first_name"
        last_name = "last_name"

        costumer = costumer_service.add_update(email=email, first_name=first_name, last_name=last_name,
                                               store=store_id, password=password)
        self.assertIsNotNone(costumer)
        self.assertEqual(costumer['email'], email)
        self.assertEqual(costumer['first_name'], first_name)
        self.assertEqual(costumer['last_name'], last_name)

        cart = shopping_cart_service.create_shopping_cart(costumer['id'])
        self.assertIsNotNone(cart)

        product = product_service.add_update(name='product', reference='ref')
        self.assertIsNotNone(product)
        self.assertEqual(product['name'], 'product')
        self.assertEqual(product['reference'], 'ref')

        shopping_cart_service.add_product_to_cart(cart['id'], product['reference'])

        sale1 = sale_service.add_sale(costumer["id"], 39.99, 'PENDING')
        self.assertEqual(sale1['costumer'], costumer["id"])
        self.assertEqual(sale1['total'], 39.99)
        self.assertEqual(sale1['status'], 'PENDING')

        sale1 = sale_service.set_sale_status_to_paid(sale1["uuid"])
        self.assertEqual(sale1['status'], 'PAID')

        costumer2 = costumer_service.add_update(email='a', first_name='a', last_name='a', store=1, password='a')
        cart2 = shopping_cart_service.create_shopping_cart(costumer2['id'])
        product2 = product_service.add_update(name='product2', reference='ref2')
        product3 = product_service.add_update(name='product3', reference='ref3')
        product4 = product_service.add_update(name='product4', reference='ref4')
        shopping_cart_service.add_product_to_cart(cart2['id'], product2['reference'])
        # shopping_cart_service.add_product_to_cart(cart2['id'], product3['reference'])
        # shopping_cart_service.add_product_to_cart(cart2['id'], product4['reference'])
        sale2 = sale_service.add_sale(costumer2["id"], 9.99, 'PENDING')
        shopping_cart_service.add_product_to_cart(cart2['id'], product2['reference'])
        sale3 = sale_service.add_sale(costumer2["id"], 9.99, 'PENDING')
        shopping_cart_service.add_product_to_cart(cart2['id'], product2['reference'])
        sale4 = sale_service.add_sale(costumer2["id"], 9.99, 'PENDING')
        shopping_cart_service.add_product_to_cart(cart2['id'], product2['reference'])
        sale5 = sale_service.add_sale(costumer2["id"], 9.99, 'PENDING')

        sale2 = sale_service.set_sale_status_to_paid(sale2["uuid"])
        sale3 = sale_service.set_sale_status_to_paid(sale3["uuid"])
        sale5 = sale_service.set_sale_status_to_paid(sale5["uuid"])

        sales = sale_service.get_sales_to_process()
        self.assertIsNotNone(sales)
        self.assertEqual(len(sales), 4)
        self.assertEqual(sales[0], sale1)
        self.assertEqual(sales[1], sale2)
        self.assertEqual(sales[2], sale3)
        self.assertEqual(sales[3], sale5)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(SaleServiceTest)
    unittest.TextTestRunner().run(suite)