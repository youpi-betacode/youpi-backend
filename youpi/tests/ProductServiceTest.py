import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.ProductService import ProductService


class ProductServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        product_service = self.injector.get(ProductService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Product)
        self.assertIsNotNone(product_service)

    def testAddUpdateProduct(self):

        product_service = self.injector.get(ProductService)

        name = "produto teste"
        description = "description"

        product = product_service.add_update(name=name, description=description)

        self.assertIsNotNone(product)
        self.assertIsNotNone(product["reference"])
        self.assertEqual(name, product["name"])
        self.assertEqual(description, product["description"])

        new_name = "xpto"

        product_updated = product_service.add_update(name="xpto", reference=product["reference"])

        self.assertEqual(product_updated["name"], new_name)
        self.assertEqual(product_updated["reference"], product["reference"])

        name = "add por refencia"
        reference = "nova referencia"

        product_by_reference = product_service.add_update(name=name, reference=reference)

        self.assertIsNotNone(product_by_reference)
        self.assertEqual(product_by_reference["reference"], reference)

    def test_add_variant(self):

        product_service = self.injector.get(ProductService)
        name1 = "produto teste 1"
        description1 = "description 1"

        product1 = product_service.add_update(name=name1, description=description1, reference="p1")

        name2 = "produto teste 2"
        description2 = "description 2"

        product2 = product_service.add_update(name=name2, description=description2, reference="p2")

        product1 = product_service.add_variant(product1["reference"], product2["reference"])

        self.assertEqual(product1["variants"][0]["reference"], product2["reference"])


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(ProductServiceTest)
    unittest.TextTestRunner().run(suite)
