import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.UserService import UserService
from pony.orm.core import ObjectNotFound
from youpi.commons.exceptions import NonMatchingPassword


class UserServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)
   
    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        user_service = self.injector.get(UserService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.User)
        self.assertIsNotNone(user_service)

    def testRecoverPassword(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        password = "password123"
        email = "email@email.email"
        first_name = "first_name"
        last_name = "last_name"
        role = 3
        store = 1
        username = "username"

        user = user_service.add_update(email=email, password=password, first_name=first_name, last_name=last_name, role=role, store=store)
        self.assertIsNotNone(user["id"])
        new_password=user_service.recover_password(email=email)
        self.assertNotEqual(new_password,password)

    def testCreateUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        password = "password"
        email = "email@email.email"
        first_name = "first_name"
        last_name = "last_name"
        role = 2
        store = 1
        username = "username"

        user = user_service.add_update(email, password, first_name, last_name, role, store, username)
        self.assertIsNotNone(user["id"])
        self.assertEqual(user["email"], email)
        self.assertEqual(user["first_name"], first_name)
        self.assertEqual(user["last_name"], last_name)
        self.assertEqual(user["role"], role)
        self.assertEqual(user["store"], store)
        self.assertEqual(user["username"], username)
        self.assertIsNotNone(user["store"])

    def testAuthenticateUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        password = "password"
        email = "email@email.email"
        first_name = "first_name"
        last_name = "last_name"
        role = 2
        store = 1

        user = user_service.add_update(email, password, first_name, last_name, role, store)
        user_auth = user_service.authenticate(email, password)
        self.assertEqual(user["id"], user_auth["id"])

        with self.assertRaises(NonMatchingPassword):
            user_service.authenticate(email, "dummy_password")

    def testUpdateUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        password = "password"
        email = "email@email.email"
        first_name = "first_name"
        last_name = "last_name"
        role = 2
        store = 1

        user = user_service.add_update(email, password, first_name, last_name, role, store)

        password = "new_password"
        email = "new_email@new_email.new_email"
        first_name = "new_first_name"
        last_name = "new_last_name"
        role = 2
        username = "new_username"

        user_updated = user_service.add_update(email, password, first_name, last_name, role, store, username, user["id"])
        self.assertIsNotNone(user_updated["id"])
        self.assertEqual(user["id"], user_updated["id"])
        self.assertEqual(user_updated["email"], email)
        self.assertEqual(user_updated["first_name"], first_name)
        self.assertEqual(user_updated["last_name"], last_name)
        self.assertEqual(user_updated["role"], role)
        self.assertEqual(user_updated["username"], username)
        self.assertIsNotNone(user["store"])

    def testUpdateNonExistingUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        with self.assertRaises(ObjectNotFound):
            user_service.add_update("email", "pass", "first", "last", 3, 1, "blabla", 10000)

    def testDeleteUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        user = user_service.get_user_by_id(1)
        self.assertIsNotNone(user)

        deleted_user = user_service.delete_user(user["id"])
        self.assertIsNotNone(deleted_user)

        with self.assertRaises(ObjectNotFound):
            user_service.get_user_by_id(deleted_user["id"])

    def testDeleteNonExistingUser(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        with self.assertRaises(ObjectNotFound):
            user_service.delete_user(1000000)

    def testGetUsersByStore(self):
        user_service = self.injector.get(UserService)
        self.assertIsNotNone(user_service)

        users = user_service.get_users_by_store(1)
        self.assertIsNotNone(users)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(UserServiceTest)
    unittest.TextTestRunner().run(suite)
