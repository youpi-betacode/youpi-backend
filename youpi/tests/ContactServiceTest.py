import os
import unittest
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.ContactService import ContactService


class ContactServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):

            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        contact_service = self.injector.get(ContactService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Contact)
        self.assertIsNotNone(contact_service)

    def testCreateContact(self):
        contact_service = self.injector.get(ContactService)
        self.assertIsNotNone(contact_service)

        first_name = "Manuel"
        last_name = "Maria"
        email = "mama@gmail.com"
        phone_number = "910401340"
        address = "Rua da Vizinha"
        birth_date = "31/02/1146"
        segment = "Singular"
        status = "Active"

        contact = contact_service.add_contact(first_name, last_name, email, phone_number, address,
                                              birth_date, segment, status)
        self.assertIsNotNone(contact["id"])
        self.assertEqual(contact["first_name"], first_name)
        self.assertEqual(contact["last_name"], last_name)
        self.assertEqual(contact["email"], email)
        self.assertEqual(str(contact["phone_number"]), phone_number)
        self.assertEqual(contact["address"], address)
        self.assertEqual(contact["birth_date"], birth_date)
        self.assertEqual(contact["segment"], segment)
        self.assertEqual(contact["status"], status)
        self.assertIsNone(contact["company"])
        self.assertIsNone(contact["business_area"])
        self.assertIsNone(contact["local"])

    def testGetAllContacts(self):
        contact_service = self.injector.get(ContactService)
        self.assertIsNotNone(contact_service)

        for i in range(5):
            example = {
                "first_name": "Manuel" + repr(i),
                "last_name": "Maria" + repr(i),
                "email": "email" + repr(i),
                "phone_number": "11100" + repr(i),
                "address": "rua" + repr(i),
                "birth_date": "12" + repr(i),
                "segment": "singular" + repr(i),
                "status": "active" + repr(i)
            }
            contact_service.add_contact(**example)

        contacts = contact_service.get_all_contacts(None, None, None, None)
        self.assertIsNotNone(contacts)

        for contact in contacts["contacts"]:
            self.assertIsNotNone(contact["id"])
            self.assertIsNotNone(contact["first_name"])
            self.assertIsNotNone(contact["first_name"])
            self.assertIsNotNone(contact["last_name"])
            self.assertIsNotNone(contact["email"])
            self.assertIsNotNone(contact["phone_number"])
            self.assertIsNotNone(contact["address"])
            self.assertIsNotNone(contact["birth_date"])
            self.assertIsNotNone(contact["segment"])
            self.assertIsNotNone(contact["status"])


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(ContactServiceTest)
    unittest.TextTestRunner().run(suite)
