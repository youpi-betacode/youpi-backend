import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.integration import INTEGRATIONS
from youpi.integration.DriveFX import DriveFXService
from youpi.services.ProductService import ProductService
from youpi.commons.exceptions import DriveFxNotAuthorized


class DriveFXServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

            for integration in INTEGRATIONS:
                binder.bind(integration, scope=singleton)

        self.injector = Injector(modules=[configure])
        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        drivefx_service = self.injector.get(DriveFXService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(drivefx_service)

    def testDriveFXAuth(self):
        drivefx_service = self.injector.get(DriveFXService)

        drivefx_service.user = "tfcmarques98@gmail.com"
        drivefx_service.backend = "https://sis08.drivefx.net/794D265E"
        drivefx_service.appId = "ABF5746AB5"
        drivefx_service.password = "passwordyoupi"
        drivefx_service.endpoint = "https://api.drivefx.net/v3"

        response = drivefx_service.auth()

        self.assertIsNotNone(response)
        self.assertEqual(response["code"], 0)
        self.assertIsNotNone(response["token"])

        drivefx_service.user = "user errado"

        self.assertRaises(DriveFxNotAuthorized, drivefx_service.auth)

    def testDriveFXStocks(self):

        drivefx_service = self.injector.get(DriveFXService)

        drivefx_service.user = "tfcmarques98@gmail.com"
        drivefx_service.backend = "https://sis08.drivefx.net/794D265E"
        drivefx_service.appId = "ABF5746AB5"
        drivefx_service.password = "passwordyoupi"
        drivefx_service.endpoint = "https://api.drivefx.net/v3"

        response = drivefx_service.get_stocks()

        self.assertIsNotNone(response)

    def testDriveFXSyncProducts(self):

        drivefx_service = self.injector.get(DriveFXService)
        product_service = self.injector.get(ProductService)

        drivefx_service.user = "tfcmarques98@gmail.com"
        drivefx_service.backend = "https://sis08.drivefx.net/794D265E"
        drivefx_service.appId = "ABF5746AB5"
        drivefx_service.password = "passwordyoupi"
        drivefx_service.endpoint = "https://api.drivefx.net/v3"

        drivefx_service.sync_products()

        products = product_service.get_products_by_store()

        self.assertIsNotNone(products)
        self.assertIsNotNone(products["count"])
        self.assertIsNotNone(products["products"])


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(DriveFXServiceTest)
    unittest.TextTestRunner().run(suite)
