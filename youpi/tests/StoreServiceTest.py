import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.StoreService import StoreService
from pony.orm.core import ObjectNotFound


class StoreServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        store_service = self.injector.get(StoreService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Store)
        self.assertIsNotNone(store_service)

    def testCreateStore(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        store_name = "Walmart Almada"
        description = "Small market place in the city of Almada"

        store = store_service.add_update(store_name, description)
        self.assertIsNotNone(store["id"])
        self.assertEqual(store["name"], store_name)
        self.assertEqual(store["description"], description)
        self.assertIsNotNone(store["extra"])
        self.assertIsNotNone(store["products"])

    def testUpdateStore(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        store_name = "Walmart Almada"
        description = "Small market place in the city of Almada"
        org_name = "Walmart"

        store = store_service.add_update(store_name, description)

        store_name = "Target Almada"
        description = "Changed name, still shit quality"

        store_updated = store_service.add_update(store_name, description, store["id"])
        self.assertIsNotNone(store_updated["id"])
        self.assertEqual(store_updated["id"], store["id"])
        self.assertEqual(store_updated["name"], store_name)
        self.assertEqual(store_updated["description"], description)
        self.assertIsNotNone(store_updated["extra"])
        self.assertIsNotNone(store_updated["products"])

    def testUpdateNonExistingStore(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        with self.assertRaises(ObjectNotFound):
            store_service.add_update("name", "description", 100000)

    def testDeleteStore(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        store = store_service.get_store_by_id(1)
        self.assertIsNotNone(store)

        deleted_store = store_service.delete_store(store["id"])
        self.assertIsNotNone(deleted_store)

        with self.assertRaises(ObjectNotFound):
            store_service.get_store_by_id(deleted_store["id"])

    def testDeleteNonExistingStore(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        with self.assertRaises(ObjectNotFound):
            store_service.delete_store(20000)

    def testGetAllStores(self):
        store_service = self.injector.get(StoreService)
        self.assertIsNotNone(store_service)

        stores = store_service.get_all_stores()
        self.assertIsNotNone(stores)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(StoreServiceTest)
    unittest.TextTestRunner().run(suite)