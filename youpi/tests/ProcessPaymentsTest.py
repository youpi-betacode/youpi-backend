import os
import unittest
from injector import Injector
from flask_injector import singleton
from youpi.services import SERVICES
from youpi.models.entities import Database
from youpi.scripts.ProcessPayments import ProcessPayments
from youpi.services.SaleService import SaleService
from youpi.services.CostumerService import CostumerService
from youpi.services.ShoppingCartService import ShoppingCartService
from youpi.services.ProductService import ProductService


class ProcessPaymentsTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            args = {
                "provider": "sqlite",
                "filename": os.path.join("..", "..", "data", "test-database.sqlite"),
                "create_db": True
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])
        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        process_payments = self.injector.get(ProcessPayments)

        self.assertIsNotNone(db)
        self.assertIsNotNone(process_payments)

    def testProcessPaymentsWithMB(self):
        process_payments = self.injector.get(ProcessPayments)
        costumer_service = self.injector.get(CostumerService)
        product_service = self.injector.get(ProductService)
        shopping_cart_service = self.injector.get(ShoppingCartService)
        sale_service = self.injector.get(SaleService)

        # create dummy sales
        dummy = costumer_service.add_update(email="email", first_name="first", last_name="last", password="password")
        dummy_cart = shopping_cart_service.create_shopping_cart(dummy["id"])
        dummy_product = product_service.add_update(name="dummy product", price_base=100, reference="ref123")

        shopping_cart_service.add_update_quantity_product_cart(dummy_cart["uuid"], dummy_product["reference"], 1)
        sale_service.create_sale(dummy["id"], 'multibanco', dummy_cart["uuid"])

        # process payments
        process_payments.process_payments()

    def testProcessPaymentsWithPayshop(self):
        process_payments = self.injector.get(ProcessPayments)
        costumer_service = self.injector.get(CostumerService)
        product_service = self.injector.get(ProductService)
        shopping_cart_service = self.injector.get(ShoppingCartService)
        sale_service = self.injector.get(SaleService)

        # create dummy sales
        dummy = costumer_service.add_update(email="email", first_name="first", last_name="last", password="password")
        dummy_cart = shopping_cart_service.create_shopping_cart(dummy["id"])
        dummy_product = product_service.add_update(name="dummy product", price_base=100, reference="ref123")

        shopping_cart_service.add_update_quantity_product_cart(dummy_cart["uuid"], dummy_product["reference"], 1)
        sale_service.create_sale(dummy["id"], 'payshop', dummy_cart["uuid"])

        # process payments
        process_payments.process_payments()

    def testProcessPaymentsForcingPaidStatus(self):
        # process_payments = self.injector.get(ProcessPayments)
        costumer_service = self.injector.get(CostumerService)
        product_service = self.injector.get(ProductService)
        shopping_cart_service = self.injector.get(ShoppingCartService)
        sale_service = self.injector.get(SaleService)

        # create dummy sales
        dummy = costumer_service.add_update(email="email", first_name="first", last_name="last", password="password")
        dummy_cart = shopping_cart_service.create_shopping_cart(dummy["id"])
        dummy_product = product_service.add_update(name="dummy product", price_base=100, reference="ref123")

        shopping_cart_service.add_update_quantity_product_cart(dummy_cart["uuid"], dummy_product["reference"], 1)
        sale_service.create_sale(dummy["id"], 'payshop', dummy_cart["uuid"])

        # process payments
        pending_sales = sale_service.get_pending_sales()

        for sale in pending_sales:
            sale = sale_service.change_sale_status_to_paid(sale["uuid"])
            self.assertEqual(sale["status"], Database.SALE_STATUS_PAID)