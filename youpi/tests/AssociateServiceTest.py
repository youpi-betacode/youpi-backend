import unittest
import os
import datetime
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.AssociateService import AssociateService
from pony.orm.core import ObjectNotFound, ExprEvalError
from youpi.commons.exceptions import InvalidAssociation


class AssociateServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        associate_service = self.injector.get(AssociateService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Associate)
        self.assertIsNotNone(associate_service)

    def testCreateAssociate(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        email = "some_email"
        password = "something_pass"
        store = 1
        first_name = "Samicas"
        last_name = "Tolo"
        associate_type = "Sponsor"
        name = "something"
        contract_exp = datetime.date(2019, 12, 24)

        associate = associate_service.add_update(email, password, first_name, last_name, store, associate_type, name,
                                                 contract_exp)
        self.assertIsNotNone(associate)
        self.assertEqual(associate["email"], email)
        self.assertEqual(associate["first_name"], first_name)
        self.assertEqual(associate["last_name"], last_name)
        self.assertEqual(associate["associate_type"], associate_type)
        self.assertEqual(associate["entity_name"], name)
        self.assertEqual(associate["contract_exp"], str(contract_exp))
        self.assertIsNotNone(associate["store"])

    def testCreateAssociateWithInvalidAssociation(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        email = "some_email"
        password = "something_pass"
        store = 1
        first_name = "Samicas"
        last_name = "Tolo"
        associate_type = "Not Sponsor"
        name = "something"
        contract_exp = datetime.date(2019, 12, 24)

        with self.assertRaises(InvalidAssociation):
            associate_service.add_update(email, password, first_name, last_name, store, associate_type,
                                         name, contract_exp)

    def testUpdateAssociate(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        email = "some_email"
        password = "something_pass"
        store = 1
        first_name = "Samicas"
        last_name = "Tolo"
        associate_type = "Sponsor"
        name = "something"
        contract_exp = datetime.date(2019, 12, 24)

        associate = associate_service.add_update(email, password, first_name, last_name, store, associate_type, name,
                                                 contract_exp)

        extra = {
            "some": "stuff",
            "number": 1
        }
        local = "MonteKaptaG"
        contact = 910023112
        volume = 1234
        total_spent = 122.2

        updated_associate = associate_service.add_update(email, password, first_name, last_name, store,
                                                         associate_type, name, contract_exp, extra, local,
                                                         contact, volume, total_spent, None, None, associate["id"])
        self.assertIsNotNone(updated_associate)
        self.assertEqual(updated_associate["extra"], extra)
        self.assertEqual(updated_associate["local"], local)
        self.assertEqual(updated_associate["contact"], contact)
        self.assertEqual(updated_associate["volume"], volume)
        self.assertEqual(updated_associate["total_spent"], total_spent)

    def testDeleteAssociate(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        associate = associate_service.get_associate_by_id(4)
        self.assertIsNotNone(associate)

        deleted_associate = associate_service.delete_associate(associate["id"])
        self.assertIsNotNone(deleted_associate)

        with self.assertRaises(ObjectNotFound):
            associate_service.get_associate_by_id(deleted_associate["id"])

    def testDeleteNonExistingAssociate(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        with self.assertRaises(ObjectNotFound):
            associate_service.delete_associate(10000)

    def testGetAssociatesByStore(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        associates = associate_service.get_associates_by_store(1, None, None, None, None)
        self.assertIsNotNone(associates)

    def testGetAssociatesFromNonExistingStore(self):
        associate_service = self.injector.get(AssociateService)
        self.assertIsNotNone(associate_service)

        with self.assertRaises(ExprEvalError):
            associate_service.get_associates_by_store(2000, None, None, None, None)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(AssociateServiceTest)
    unittest.TextTestRunner().run(suite)
