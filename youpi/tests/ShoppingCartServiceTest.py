import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.ShoppingCartService import ShoppingCartService
from youpi.services.ProductService import ProductService
from youpi.services.CostumerService import CostumerService


class ShoppingCartServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        cart_service = self.injector.get(ShoppingCartService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.ShoppingCart)
        self.assertIsNotNone(cart_service)

    def testCreateCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        product_service = self.injector.get(ProductService)

        shopping_cart = cart_service.create_shopping_cart()

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])

    def testAddCartToCostumer(self):
        cart_service = self.injector.get(ShoppingCartService)
        costumer_service = self.injector.get(CostumerService)

        costumer = costumer_service.add_update(email="mail@mail.com", first_name="first name", last_name="last name", password="xpto")
        costumer2 = costumer_service.add_update(email="mail2@mail.com", first_name="first name", last_name="last name", password="xpto")


        shopping_cart = cart_service.create_shopping_cart()

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])

        shopping_cart = cart_service.add_costumer_to_shopping_cart(uuid=shopping_cart["uuid"], costumer_id=costumer["id"])

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])
        self.assertEqual(shopping_cart["costumer"]["email"], "mail@mail.com")

        shopping_cart = cart_service.create_shopping_cart(costumer_id=costumer["id"])

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])
        self.assertEqual(shopping_cart["costumer"]["email"], "mail@mail.com")

        shopping_cart = cart_service.create_shopping_cart(costumer_id=costumer2["id"])

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])
        self.assertEqual(shopping_cart["costumer"]["email"], "mail2@mail.com")

    def testIncrementeDecrementProductsCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        product_service = self.injector.get(ProductService)

        shopping_cart = cart_service.create_shopping_cart()

        product1 = product_service.add_update(name="My Product1", reference="my_product1")
        product2 = product_service.add_update(name="My Product2", reference="my_product2")

        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product1",
                                                            extra={"info": "this is some extra info about the product"})
        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product1",
                                                            extra={"info": "this is some extra info about the product"})
        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product2",
                                                            extra={"info": "this is some extra info about the product"})
        shopping_cart = cart_service.decrement_product_cart(uuid=shopping_cart["uuid"], reference="my_product2")

        self.assertIsNotNone(shopping_cart)
        self.assertIn("uuid", shopping_cart)
        self.assertIsNotNone(shopping_cart["uuid"])
        self.assertEqual(len(shopping_cart["products"]), 1)

        shopping_cart = cart_service.delete_product_cart(reference="my_product1", uuid=shopping_cart["uuid"])

        self.assertEqual(len(shopping_cart["products"]), 0)

        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product1",
                                                            extra={"info": "this is some extra info about the product"})

        shopping_cart = cart_service.add_update_quantity_product_cart(reference="my_product1", uuid=shopping_cart["uuid"], amount=10)

        self.assertEqual(shopping_cart["products"][0]["amount"], 10)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(ShoppingCartServiceTest)
    unittest.TextTestRunner().run(suite)
