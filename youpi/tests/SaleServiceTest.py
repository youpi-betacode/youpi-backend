import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.SaleService import SaleService
from youpi.services.StoreService import StoreService
from youpi.integration.DriveFX import DriveFXService
from youpi.services.ProductService import ProductService
from youpi.services.CostumerService import CostumerService
from youpi.services.ShoppingCartService import ShoppingCartService
from pony.orm.core import ObjectNotFound, ExprEvalError
from youpi.commons.exceptions import EmptyShoppingCart


class SaleServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

        store_service = self.injector.get(StoreService)
        drivefx_service = self.injector.get(DriveFXService)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        sale_service = self.injector.get(SaleService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Sale)
        self.assertIsNotNone(sale_service)

    def testCreateSale(self):
        sale_service = self.injector.get(SaleService)
        shopping_cart_service = self.injector.get(ShoppingCartService)
        product_service = self.injector.get(ProductService)
        cart_service = self.injector.get(ShoppingCartService)
        costumer_service = self.injector.get(CostumerService)

        costumer = costumer_service.add_update(email="test@test.com", first_name="fist_name", last_name="last_name", password="xpto")

        shopping_cart = cart_service.create_shopping_cart()

        product1 = product_service.add_update(name="My Product1", reference="my_product1", price_base=10, price_promo=10)
        product2 = product_service.add_update(name="My Product2", reference="my_product2", price_base=20, price_promo=20)
        product3 = product_service.add_update(name="YSECASA", reference="YSECASA", price_promo=10)

        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product1",
                                                            extra={"info": "this is some extra info about the product"})
        shopping_cart = cart_service.increment_product_cart(uuid=shopping_cart["uuid"], reference="my_product1",
                                                            extra={"info": "this is some extra info about the product"})

        sale = sale_service.create_sale(costumer=costumer["id"], payment=SaleService.PAYMENT_TYPE_MULTIBANCO, shopping_cart_uuid=shopping_cart["uuid"])

        self.assertIsNotNone(sale)
        self.assertIn("payment_info", sale["extra"])


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(SaleServiceTest)
    unittest.TextTestRunner().run(suite)
