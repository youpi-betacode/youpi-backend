import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.SupplierService import SupplierService
from pony.orm.core import ObjectNotFound


class SupplierServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        supplier_service = self.injector.get(SupplierService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Supplier)
        self.assertIsNotNone(supplier_service)

    def testCreateSupplier(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        supplier_name = "Amazon"
        description = "It's a supplier! What do you need to know more?"
        address = "In yo momma!"
        phone_number = 969696969
        email = "email@email.email"

        supplier = supplier_service.add_update(supplier_name, description, address, phone_number, email)
        self.assertIsNotNone(supplier["id"])
        self.assertEqual(supplier["name"], supplier_name)
        self.assertEqual(supplier["description"], description)
        self.assertEqual(supplier["address"], address)
        self.assertEqual(supplier["phone_number"], phone_number)
        self.assertEqual(supplier["email"], email)
        self.assertIsNotNone(supplier["extra"])
        self.assertIsNotNone(supplier["products"])

    def testUpdateSupplier(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        supplier_name = "Amazon"
        description = "It's a supplier! What do you need to know more?"
        address = "In yo momma!"
        phone_number = 969696969
        email = "email@email.email"

        supplier = supplier_service.add_update(supplier_name, description, address, phone_number, email)

        supplier_name = "AliExpress"
        description = "It's a Chinese supplier! Dont expect quality!"
        address = "Still in yo momma!"
        phone_number = 917777777
        email = "new_email@email.email"

        supplier_updated = supplier_service.add_update(supplier_name, description, address, phone_number,
                                                       email, supplier["id"])
        self.assertIsNotNone(supplier_updated["id"])
        self.assertEqual(supplier_updated["name"], supplier_name)
        self.assertEqual(supplier_updated["description"], description)
        self.assertEqual(supplier_updated["address"], address)
        self.assertEqual(supplier_updated["phone_number"], phone_number)
        self.assertEqual(supplier_updated["email"], email)
        self.assertIsNotNone(supplier_updated["extra"])
        self.assertIsNotNone(supplier_updated["products"])

    def testUpdateNonExistingSupplier(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        with self.assertRaises(ObjectNotFound):
            supplier_service.add_update("name", "desc", "address", 909090909, "email", 10000)

    def testDeleteSupplier(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        supplier = supplier_service.get_supplier_by_id(1)
        self.assertIsNotNone(supplier)

        deleted_supplier = supplier_service.delete_supplier(supplier["id"])
        self.assertIsNotNone(deleted_supplier)

        with self.assertRaises(ObjectNotFound):
            supplier_service.get_supplier_by_id(deleted_supplier["id"])

    def testDeleteNonExistingSupplier(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        with self.assertRaises(ObjectNotFound):
            supplier_service.delete_supplier(100000)

    def testGetAllSuppliers(self):
        supplier_service = self.injector.get(SupplierService)
        self.assertIsNotNone(supplier_service)

        suppliers = supplier_service.get_all_suppliers()
        self.assertIsNotNone(suppliers)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(SupplierServiceTest)
    unittest.TextTestRunner().run(suite)