import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.ShoppingCartService import ShoppingCartService
from youpi.services.CostumerService import CostumerService
from pony.orm.core import ObjectNotFound


class ShoppingCartServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        cart_service = self.injector.get(ShoppingCartService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.ShoppingCart)
        self.assertIsNotNone(cart_service)

    def testCreateCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(cart_service)
        self.assertIsNotNone(costumer_service)

        email = "email@email.email.io"
        password = "something"
        first_name = "forest"
        last_name = "gump"
        store = 1

        costumer = costumer_service.add_update(email, password, first_name, last_name, store)
        cart = cart_service.add_update(costumer["id"])
        self.assertIsNotNone(cart)
        self.assertIsNotNone(cart["costumer"])
        self.assertIsNotNone(cart["total"])

    def testUpdateCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(cart_service)
        self.assertIsNotNone(costumer_service)

        email = "email@email.email.io"
        password = "something"
        first_name = "forest"
        last_name = "gump"
        store = 1
        total = 325.69

        costumer = costumer_service.add_update(email, password, first_name, last_name, store)
        cart = cart_service.add_update(costumer["id"], total)

        total = 699.00

        updated_cart = cart_service.add_update(costumer["id"], total, cart["id"])
        self.assertIsNotNone(updated_cart)
        self.assertIsNotNone(updated_cart["costumer"])
        self.assertEqual(updated_cart["costumer"], cart["costumer"])
        self.assertEqual(updated_cart["total"], total)

    def testUpdateNonExistingCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        self.assertIsNotNone(cart_service)

        with self.assertRaises(ObjectNotFound):
            cart_service.add_update(1, 200, 10000)

    def testDeleteCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        self.assertIsNotNone(cart_service)

        cart = cart_service.get_cart_by_costumer(3)
        self.assertIsNotNone(cart)

        deleted_cart = cart_service.delete_cart(cart["id"])
        self.assertIsNotNone(deleted_cart)

        non_existing_cart = cart_service.get_cart_by_costumer(1)
        self.assertIsNone(non_existing_cart)

    def testDeleteNonExistingCart(self):
        cart_service = self.injector.get(ShoppingCartService)
        self.assertIsNotNone(cart_service)

        with self.assertRaises(ObjectNotFound):
            cart_service.delete_cart(10000)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(ShoppingCartServiceTest)
    unittest.TextTestRunner().run(suite)