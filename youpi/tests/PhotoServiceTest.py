import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.PhotoService import PhotoService


class PhotoServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        photo_service = self.injector.get(PhotoService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Photo)
        self.assertIsNotNone(photo_service)

    def testUploadPhotoToProduct(self):
        photo_service = self.injector.get(PhotoService)
        self.assertIsNotNone(photo_service)

        image = os.path.join("..", "..", "data", "test-image.jpg")

        photo_service.upload_photo_to_product(image, 1)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(PhotoServiceTest)
    unittest.TextTestRunner().run(suite)