import unittest
import os
from flask_injector import singleton
from injector import Injector
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.CostumerService import CostumerService
from pony.orm.core import ObjectNotFound, ExprEvalError, TransactionIntegrityError


class CostumerServiceTest(unittest.TestCase):
    def setUp(self):
        def configure(binder):
            PROVIDER = "sqlite"
            FILE_NAME = os.path.join("..", "..", "data", "test-database.sqlite")
            create_db = True

            args = {
                "provider": PROVIDER,
                "filename": FILE_NAME,
                "create_db": create_db
            }

            db = Database(**args)
            db.model.generate_mapping(create_tables=True)
            binder.bind(Database, to=db, scope=singleton)

            for service in SERVICES:
                binder.bind(service, scope=singleton)

        self.injector = Injector(modules=[configure])

        self.db = self.injector.get(Database)

    def tearDown(self):
        self.db.model.drop_all_tables(with_all_data=True)

    def testEverythingIsOk(self):
        db = self.injector.get(Database)
        costumer_service = self.injector.get(CostumerService)

        self.assertIsNotNone(db)
        self.assertIsNotNone(db.model.Costumer)
        self.assertIsNotNone(costumer_service)

    def testCreateCostumer(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        password = "password"
        email = "email@email.email"
        store_id = 1
        first_name = "first_name"
        last_name = "last_name"

        costumer = costumer_service.add_update(email, password, first_name, last_name, store_id)
        self.assertIsNotNone(costumer)
        # self.assertEqual(costumer["password"], password)
        self.assertEqual(costumer["email"], email)
        self.assertIsNotNone(costumer["store"])
        self.assertEqual(costumer["first_name"], first_name)
        self.assertEqual(costumer["last_name"], last_name)

    def testCreateDuplicateCostumer(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        password = "password"
        email = "email@email.email"
        store_id = 1
        first_name = "first_name"
        last_name = "last_name"

        costumer_service.add_update(email, password, first_name, last_name, store_id)
        with self.assertRaises(TransactionIntegrityError):
            costumer_service.add_update(email, password, first_name, last_name, store_id)

    def testUpdateCostumer(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        password = "password"
        email = "email@email.email"
        store_id = 1
        first_name = "first_name"
        last_name = "last_name"

        costumer = costumer_service.add_update(email, password, first_name, last_name, store_id)

        password = "new_password"
        email = "new_email@email.email"
        first_name = "new_first_name"
        last_name = "new_last_name"
        local = "Entroncamento"
        contact = "910000000"
        volume = 5
        total_spent = 1000

        updated_costumer = costumer_service.add_update(email, password, first_name, last_name, store_id, {}, local,
                                                       contact, volume, total_spent, costumer["id"])
        self.assertIsNotNone(updated_costumer)
        # self.assertEqual(costumer["password"], password)
        self.assertEqual(updated_costumer["email"], email)
        self.assertIsNotNone(updated_costumer["store"])
        self.assertEqual(updated_costumer["first_name"], first_name)
        self.assertEqual(updated_costumer["last_name"], last_name)
        self.assertEqual(updated_costumer["local"], local)
        self.assertEqual(updated_costumer["contact"], contact)
        self.assertEqual(updated_costumer["volume"], volume)

    def testDeleteCostumer(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        costumer = costumer_service.get_costumer_by_id(3)
        self.assertIsNotNone(costumer)

        deleted_costumer = costumer_service.delete_costumer(costumer["id"])
        self.assertIsNotNone(deleted_costumer)

        with self.assertRaises(ObjectNotFound):
            costumer_service.get_costumer_by_id(deleted_costumer["id"])

    def testDeleteNonExistingCostumer(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        with self.assertRaises(ObjectNotFound):
            costumer_service.delete_costumer(10000)

    def testGetCostumersByStore(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        costumers = costumer_service.get_costumers_by_store(1, None, None, None, None)
        self.assertIsNotNone(costumers)

    def testGetCostumersFromNonExistingStore(self):
        costumer_service = self.injector.get(CostumerService)
        self.assertIsNotNone(costumer_service)

        with self.assertRaises(ExprEvalError):
            costumer_service.get_costumers_by_store(2000, None, None, None, None)


if __name__ == "__main__":
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(CostumerServiceTest)
    unittest.TextTestRunner().run(suite)