from flask_injector import inject
from youpi.models.entities import Database
from youpi.commons.exceptions import DriveFxNotAuthorized
from youpi.services.ProductService import ProductService
import requests
import os
import urllib.parse


class DriveFXService:
    @inject
    def __init__(self, db: Database, product_service:ProductService):
        self.db = db
        self.product_service = product_service

        self.user = os.environ.get("DRIVE_FX_USER", "tfcmarques98@gmail.com")
        self.backend = os.environ.get("DRIVE_FX_BACKEND", "https://sis08.drivefx.net/794D265E")
        self.appId = os.environ.get("DRIVE_FX_BACKEND", "ABF5746AB5")
        self.password = os.environ.get("DRIVE_FX_PASSWORD", "passwordyoupi")
        self.endpoint = os.environ.get("DRIVE_FX_ENDPOINT", "https://api.drivefx.net/v3")
        self.paid_delivery = os.environ.get("PAID_DELIVERY", 'YSECASA')
        self.free_delivery = os.environ.get("FREE_DELIVERY", 'YSECASAGRATUITO')
        self.token = None
        self.promo = os.environ.get("PRODUCT_PROMO", "0.3")

    def auth(self):
        url = f"{self.endpoint}/generateAccessToken"
        body = {
            "credentials": {
                "backendUrl": self.backend,
                "appId": self.appId,
                "userCode": self.user,
                "password": self.password
            }
        }

        response = requests.post(url, json=body)
        response_json = response.json()

        if response_json["code"] == 0:
            self.token = response_json["token"]
        else:
            raise DriveFxNotAuthorized()

        return response_json

    def get_stocks(self):

        if self.token is None:
            self.auth()

        url = f"{self.endpoint}/searchEntities"
        body = {
            "queryObject": {
                "distinct": True,
                "groupByItems": [],
                "orderByItems": [],
                "SelectItems": [],
                "entityName": "St",
                "filterItems": []
            }
        }
        headers = {
            "Authorization": self.token
        }

        response = requests.post(url, json=body, headers=headers)
        response_json = response.json()

        if response_json["code"] == 0:
            return response_json["entities"]
        else:
            raise DriveFxNotAuthorized()

    def sync_products(self):

        products = self.get_stocks()
        imported_products = []
        for product in products:

            reference = product["ref"]
            name = product["design"]
            description = product["desctec"]
            category = product["familia"]
            sub_category = product["usr1"]
            stock = product["stock"]
            brand = sub_category
            model = product["usr2"]

            price1 = product["epv1"]
            price2 = product["epv2"]
            price3 = product["epv3"]
            price4 = product["epv4"]
            price5 = product["epv5"]

            price1tax = product["epv1comiva"]
            price2tax = product["epv2comiva"]
            price3tax = product["epv3comiva"]
            price4tax = product["epv4comiva"]
            price5tax = product["epv5comiva"]

            extra_info = product["obs"]

            photo = f"{self.backend}/PHCWS/cimagem.aspx?iflstamp={urllib.parse.quote(product['imagestamp'])}&rand=0.7403755064897029" if product['imagestamp'] else None

            extra = {
                "brand": brand,
                "model": model,
                "extra_info": extra_info,
                "photo": photo,
                "discount_percentage": self.promo,
                "inactive": product['inactivo']
                #"driveFX": product
            }

            price_promo = round(price1*(1-float(self.promo)), 2)

            if reference == self.paid_delivery or reference == self.free_delivery:
                price_promo = price1

            imported_product = self.product_service.add_update(name=name,
                                            extra=extra,
                                            description=description,
                                            reference=reference,
                                            category=category,
                                            price_base=price1,
                                            stock=stock,
                                            sub_category=sub_category,
                                            price_promo=price_promo
                                            )

            imported_products.append(imported_product)

        for product in products:
            if "scs" in product:
                for pc in product["scs"]:
                    self.product_service.add_variant(product['ref'], pc['ref'])

        return imported_products
