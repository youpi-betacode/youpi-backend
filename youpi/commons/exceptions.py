class InvalidToken(Exception):
    pass


class AccessDenied(Exception):
    pass


class InvalidRole(Exception):
    pass


class InvalidAssociation(Exception):
    pass


class NonMatchingPassword(Exception):
    pass


class MissingPassword(Exception):
    pass


class EmptyShoppingCart(Exception):
    pass


class MissingVariantFromCart(Exception):
    pass


class LoadedCartIsEmpty(Exception):
    pass


class NoPhotosFound(Exception):
    pass


class InvalidVariant(Exception):
    pass


class AlreadyAssignedPhoto(Exception):
    pass


class UnassignedPhoto(Exception):
    pass


class PaymentRequestFailed(Exception):
    def __init__(self, error):
        self.error = error


class PaymentInfoRequestFailed(Exception):
    def __init__(self, error):
        self.error = error


class EmailFailed(Exception):
    def __init__(self, error):
        self.error = error


class FileManagerUploadError(Exception):
    def __init__(self, error):
        self.error = error


class FileManagerDeleteError(Exception):
    def __init__(self, error):
        self.error = error


class RepeatedContacts(Exception):
    def __init__(self, index):
        self.index = index


class EmailRequired(Exception):
    def __init__(self, index):
        self.index = index


class DriveFxNotAuthorized(Exception):
    pass


class ApplicationException(Exception):
    def __init__(self, msg):
        self.msg = msg
