import logging


class ExceptionHandler:
    @staticmethod
    def obj_not_found():
        logging.warning("Object was not found.")
        return {"message": "Object not found."}, 404

    @staticmethod
    def expr_eval_error():
        logging.warning("Invalid parameters.")
        return {"message": "Invalid parameters."}, 400

    @staticmethod
    def transaction_integrity_error():
        logging.warning("Duplicated unique constraint.")
        return {"message": "Duplicated unique constraint."}, 400

    @staticmethod
    def invalid_credentials():
        logging.warning("Invalid credentials.")
        return {"message": "Invalid credentials."}, 401

    @staticmethod
    def missing_password():
        logging.warning("Missing password while creating an user.")
        return {"messsage": "Missing password while creating an user."}, 400

    @staticmethod
    def invalid_role():
        logging.warning("Invalid role.")
        return {"message": "Invalid role."}, 400

    @staticmethod
    def invalid_association():
        logging.warning("Invalid association. Check if associate type is Sponsor or Partner")
        return {"message": "Invalid association. Check if associate type is Sponsor or Partner"}, 400

    @staticmethod
    def delimiter_error():
        logging.warning("File delimiter is invalid.")
        return {"message": "File delimiter is invalid"}, 400

    @staticmethod
    def empty_cart():
        logging.warning("Can't proceed with sale if shopping cart is empty.")
        return {"message": "Can't proceed with sale if shopping cart is empty."}, 400

    @staticmethod
    def missing_variant_from_cart():
        logging.warning("Chosen variant does not exist in the costumer's shopping cart.")
        return {"message": "Chosen variant does not exist in the costumer's shopping cart."}, 400

    @staticmethod
    def loaded_cart_empty():
        logging.warning("Loaded cart from store is empty. Can't updade costumer's shopping cart.")
        return {"message": "Loaded cart from store is empty. Can't updade costumer's shopping cart."}, 400

    @staticmethod
    def no_photos_found():
        logging.warning("No photos were found for the selected object.")
        return {"message": "No photos were found for the selected object."}, 404

    @staticmethod
    def invalid_variant():
        logging.warning("Selected variant isn't from the same product as selected photo.")
        return {"message": "Selected variant isn't from the same product as selected photo."}, 400

    @staticmethod
    def already_assigned_photo():
        logging.warning("Selected photo already has a variant assigned to it.")
        return {"message": "Selected photo already has a variant assigned to it."}, 400

    @staticmethod
    def unassigned_photo():
        logging.warning("Photo has no variant assigned to remove.")
        return {"message": "Photo has no variant assigned to remove."}, 400

    @staticmethod
    def payment_request_failed(error):
        logging.warning(f"Failed payment request. Error: {error}")
        return {"message": f"Failed payment request. Error: {error}"}, 400

    @staticmethod
    def payment_info_request_failed(error):
        logging.warning(f"Failed payment info request. Error: {error}")
        return {"message": f"Failed payment info request. Error: {error}"}, 400

    @staticmethod
    def email_failed(error):
        logging.warning(f"Failed sending email. Error: {error}")
        return {"message": f"Failed sending email. Error: {error}"}, 400

    @staticmethod
    def file_manager_upload_error(error):
        logging.warning(f"File Manager couldn't upload selected file. Error: {error}")
        return {"message": f"File Manager couldn't upload selected file. Error: {error}"}, 400

    @staticmethod
    def file_manager_delete_error(error):
        logging.warning(f"File Manager couldn't delete selected file. Error: {error}")
        return {"message": f"File Manager couldn't delete selected file. Error: {error}"}, 400

    @staticmethod
    def email_required(index):
        logging.warning(f"Email is required to register a contact. Line {index} of CSV file.")
        return {"message": f"Email is required to register a contact. Line {index} of CSV file"}, 400

    @staticmethod
    def repeated_contacts(index):
        logging.warning(f"Repeated contacts in the file. Line {index} of CSV file.")
        return {"message": f"Repeated contacts in the file. Line {index} of CSV file"}, 400

