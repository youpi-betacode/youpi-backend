from functools import wraps
from flask_jwt_extended import get_jwt_identity
from flask_injector import inject
from youpi.commons.exceptions import InvalidToken, AccessDenied

ACCESS = {
    "Costumer": 0,
    "Associate": 1,
    "Collaborator": 2,
    "Manager": 3,
    "SuperAdmin": 4
}


@inject
def requires_access_level(access_level):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            user = get_jwt_identity()
            if not user:
                raise InvalidToken
            if user["role"] >= access_level:
                allowed = True
            else:
                allowed = False
            if not allowed:
                raise AccessDenied
            return f(*args, **kwargs)
        return decorated_function
    return decorator
