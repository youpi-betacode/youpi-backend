import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
from pony.orm import db_session
from pony.orm.core import ObjectNotFound
from flask_injector import inject, singleton
from injector import Injector
from youpi.services import SERVICES
from youpi.models.entities import Database
from youpi.services.StoreService import StoreService
from youpi.services.UserService import UserService


class PopulateUsers:
    @inject
    def __init__(self, db: Database, user_service: UserService, store_service: StoreService):
        self.db = db
        self.user_service = user_service
        self.store_service = store_service

    def populate_users(self):
        self.create_default_store()

        self.create_super_user()

    @db_session
    def create_default_store(self):
        try:
            self.db.model.Store[1]

        except ObjectNotFound:
            store = {
                "name": "Default Store",
                "description": "Default Description"
            }
            self.store_service.add_update(**store)

    def create_super_user(self):
        super_user = {
            "email": "superuser@email.com",
            "first_name": "Super",
            "last_name": "User",
            "role": 4,
            "store": 1,
            "extra": {},
            "password": "superuser"
        }
        self.user_service.add_update(**super_user)


if __name__ == "__main__":
    def configure(binder):
        # args = {
        #     "provider": "sqlite",
        #     "filename": os.path.join("..", "..", "data", "database.sqlite"),
        #     "create_db": True
        # }

        args = {
           "provider": "mysql",
           "host": "127.0.0.1",
           "user": "root",
           "passwd": "Pocosi12!",
           "db": "dev_youpi"
        }

        db = Database(**args)
        db.model.generate_mapping(create_tables=True)
        binder.bind(Database, to=db, scope=singleton)

        for service in SERVICES:
            binder.bind(service, scope=singleton)


    injector = Injector(modules=[configure])

    db = injector.get(Database)
    populator = injector.get(PopulateUsers)

    populator.populate_users()