#!/usr/bin/env python
# coding: latin-1
import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
import uuid
import random
from pony.orm import db_session
from pony.orm.core import ObjectNotFound
from injector import Injector
from flask_injector import inject, singleton
from youpi.services import SERVICES
from youpi.models.entities import Database
from youpi.services.ProductService import ProductService
from youpi.services.VariantService import VariantService
from youpi.services.PhotoService import PhotoService
from youpi.services.StoreService import StoreService
from youpi.services.SupplierService import SupplierService


class PopulateProducts:
    @inject
    def __init__(self, db: Database, product_service: ProductService, variant_service: VariantService,
                 photo_service: PhotoService, store_service: StoreService, supplier_service: SupplierService):
        self.db = db
        self.product_service = product_service
        self.variant_service = variant_service
        self.photo_service = photo_service
        self.store_service = store_service
        self.supplier_service = supplier_service

    def populate_products(self):
        self.create_default_store()
        self.create_default_supplier()

        for i in range(20):
            self.create_youpi(i)
            self.create_yoji(i)
            self.create_liso(i)

    @db_session
    def create_default_store(self):
        try:
            self.db.model.Store[1]

        except ObjectNotFound:
            store = {
                "name": "Default Store",
                "description": "Default Description"
            }
            self.store_service.add_update(**store)

    @db_session
    def create_default_supplier(self):
        try:
            self.db.model.Supplier[1]

        except ObjectNotFound:
            supplier = {
                "name": "Default Supplier",
                "description": "Supplier",
                "address": "Default Supplier Address",
                "phone_number": random.randrange(900000000, 1000000000),
                "email": "Default Supplier Email"
            }
            self.supplier_service.add_update(**supplier)

    def create_youpi(self, i):
        product = {
            "name": "YOUPi! MERMAID" + repr(i),
            "description": "YOUPi! Description",
            "category": "YOUPi!",
            "store": 1,
            "supplier": 1,
        }
        product = self.product_service.add_update(**product)

        price_base = random.uniform(10.0, 200.0)
        default_var = {
            "reference": "ref#" + str(uuid.uuid4())[:8],
            "label": "Moldura",
            "product": product["id"],
            "stock": 100,
            "stock_status": "Available",
            "price_base": round(price_base, 2),
            "price_promo": round(price_base / 2, 2),
            "extra": {
                "info": {
                    "size": "40x40cm",
                    "material": "YOUPi! constitu�do por duas pe�as, BEST e FEELING.",
                    "measures": {
                        "BEST": "50x50cm",
                        "FEELING": "42x42cm"
                    },
                    "warning": "Recomenda-se a limpeza com um pano limpo e seco."
                }
            }
        }
        self.variant_service.add_update(**default_var)

        urls = [
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/289x278/5c895ca8802708a0f3b0be49a97962b8/youpii3.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/233x268/0f54961dd93b7eb4863927bb6de7ed8c/youpii2.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/282x285/e4e3a22f51baff0eaf8e5e678ac4d179/youpii1.PNG.png"
        ]
        for url in urls:
            self.photo_service.add_photo_to_product(url, str(uuid.uuid4()), product["id"])

    def create_yoji(self, i):
        product = {
            "name": "YOJI! MERMAID" + repr(i),
            "description": "YOUPi! Description",
            "category": "YOJI",
            "store": 1,
            "supplier": 1,
        }
        product = self.product_service.add_update(**product)

        price_base = random.uniform(10.0, 200.0)
        default_var = {
            "reference": "ref#" + str(uuid.uuid4())[:8],
            "label": "Moldura",
            "product": product["id"],
            "stock": 100,
            "stock_status": "Available",
            "price_base": round(price_base, 2),
            "price_promo": round(price_base / 2, 2),
            "extra": {
                "info": {
                    "size": "40x40cm",
                    "material": "YOUPi! constitu�do por duas pe�as, BEST e FEELING.",
                    "measures": {
                        "BEST": "50x50cm",
                        "FEELING": "42x42cm"
                    },
                    "warning": "Recomenda-se a limpeza com um pano limpo e seco."
                }
            }
        }
        self.variant_service.add_update(**default_var)

        urls = [
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/289x278/5c895ca8802708a0f3b0be49a97962b8/youpii3.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/233x268/0f54961dd93b7eb4863927bb6de7ed8c/youpii2.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/282x285/e4e3a22f51baff0eaf8e5e678ac4d179/youpii1.PNG.png"
        ]
        for url in urls:
            self.photo_service.add_photo_to_product(url, str(uuid.uuid4()), product["id"])

    def create_liso(self, i):
        product = {
            "name": "LISO" + repr(i),
            "description": "YOUPi! Description",
            "category": "LISO",
            "store": 1,
            "supplier": 1,
        }
        product = self.product_service.add_update(**product)

        price_base = random.uniform(10.0, 200.0)
        default_var = {
            "reference": "ref#" + str(uuid.uuid4())[:8],
            "label": "Moldura",
            "product": product["id"],
            "stock": 100,
            "stock_status": "Available",
            "price_base": round(price_base, 2),
            "price_promo": round(price_base / 2, 2),
            "extra": {
                "info": {
                    "size": "40x40cm",
                    "material": "YOUPi! constitu�do por duas pe�as, BEST e FEELING.",
                    "measures": {
                        "BEST": "50x50cm",
                        "FEELING": "42x42cm"
                    },
                    "warning": "Recomenda-se a limpeza com um pano limpo e seco."
                }
            }
        }
        self.variant_service.add_update(**default_var)

        urls = [
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/289x278/5c895ca8802708a0f3b0be49a97962b8/youpii3.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/233x268/0f54961dd93b7eb4863927bb6de7ed8c/youpii2.PNG.png",
            "https://trello-attachments.s3.amazonaws.com/5db1cbde94dfed19f3cbeabd/282x285/e4e3a22f51baff0eaf8e5e678ac4d179/youpii1.PNG.png"
        ]
        for url in urls:
            self.photo_service.add_photo_to_product(url, str(uuid.uuid4()), product["id"])


if __name__ == "__main__":
    def configure(binder):
        # args = {
        #     "provider": "sqlite",
        #     "filename": os.path.join("..", "..", "data", "database.sqlite"),
        #     "create_db": True
        # }

        args = {
           "provider": "mysql",
           "host": "127.0.0.1",
           "user": "root",
           "passwd": "Pocosi12!",
           "db": "dev_youpi"
        }

        db = Database(**args)
        db.model.generate_mapping(create_tables=True)
        binder.bind(Database, to=db, scope=singleton)

        for service in SERVICES:
            binder.bind(service, scope=singleton)


    injector = Injector(modules=[configure])

    db = injector.get(Database)
    populator = injector.get(PopulateProducts)

    populator.populate_products()
