import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
import requests
import logging
from youpi.models.entities import Database
from youpi.services import SERVICES
from youpi.services.SaleService import SaleService
from youpi.integration import INTEGRATIONS
from injector import singleton, Injector, inject
from youpi.commons.exceptions import PaymentInfoRequestFailed
from youpi.commons.exception_handler import ExceptionHandler

YOUPI_DATABASE_TYPE = os.environ.get("YOUPI_DATABASE_TYPE", "mysql")
YOUPI_DATABASE_URL = os.environ.get("YOUPI_DATABASE_URL", os.path.join("..", "..", "data", "database.sqlite"))
YOUPI_DATABASE_HOST = os.environ.get("YOUPI_DATABASE_HOST", "store.youpii.pt")
YOUPI_DATABASE_USER = os.environ.get("YOUPI_DATABASE_USER", "root")
YOUPI_DATABASE_PASSWD = os.environ.get("YOUPI_DATABASE_PASSWD", "Pocosi12!")
YOUPI_DATABASE_NAME = os.environ.get("YOUPI_DATABASE_NAME", "youpi")
YOUPI_LOCALHOST_PORT = int(os.environ.get("YOUPI_LOCALHOST_PORT", "5000"))
YOUPI_DEBUG_FLAG = bool(os.environ.get("YOUPI_DEBUG_FLAG", "True"))
PAYMENT_ENDPOINT = os.environ.get("PAYMENT_ENDPOINT", "http://api.paymentgateways.betacode.tech/v1")

PROCESSABLE_PAYMENT_TYPE = ["multibanco", "payshop"]

class ProcessPayments:
    PAYMENT_TYPE_MULTIBANCO = "multibanco"
    PAYMENT_TYPE_PAYSHOP = "payshop"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    @staticmethod
    def mb_current_status(reference):
        url = f"{PAYMENT_ENDPOINT}/hipay/mb_reference_info"
        body = {
            "mb_reference": reference
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentInfoRequestFailed(response.raise_for_status())
        else:
            return response.json()

    @staticmethod
    def payshop_current_status(reference):
        url = f"{PAYMENT_ENDPOINT}/hipay/payshop_reference_info"
        body = {
            "payshop_reference": reference
        }

        response = requests.post(url, data=body)

        if response.status_code != requests.codes.ok:
            raise PaymentInfoRequestFailed(response.raise_for_status())
        else:
            return response.json()

    def check_if_paid(self, payment_type, payment_ref):
        sale_status = None

        if payment_type == ProcessPayments.PAYMENT_TYPE_MULTIBANCO:
            sale_status = self.mb_current_status(payment_ref)
        elif payment_type == ProcessPayments.PAYMENT_TYPE_PAYSHOP:
            sale_status = self.payshop_current_status(payment_ref)

        return sale_status["paid"]

    def process_payments(self):
        logging.basicConfig(level=logging.INFO)
        logging.info('Payment processing started...')
        pending_sales = self.sale_service.get_pending_sales()
        logging.info(f'Processing {len(pending_sales)} pending sales...')

        for sale in pending_sales:
            try:
                payment_type = sale["payment_type"]
                if payment_type in PROCESSABLE_PAYMENT_TYPE:
                    payment_ref = sale["extra"]["payment_info"]["reference"]

                    if self.check_if_paid(payment_type, payment_ref):
                        self.sale_service.change_sale_status_to_paid(sale["uuid"])
                        logging.info(f'Sale {sale["uuid"]} status changed to paid...')
                else:
                    logging.warning(f'Sale {sale["uuid"]} ignore because is of type {payment_type} ...')

            except PaymentInfoRequestFailed as error:
                return ExceptionHandler.payment_info_request_failed(error)

        logging.info('Payment processing finished...')


if __name__ == "__main__":
    def configure(binder):
        args = {}

        if YOUPI_DATABASE_TYPE in (None, "sqlite"):
            args = {
                "provider": "sqlite",
                "filename": YOUPI_DATABASE_URL,
                "create_db": True
            }
        elif YOUPI_DATABASE_TYPE == "mysql":
            args = {
                "provider": "mysql",
                "host": YOUPI_DATABASE_HOST,
                "user": YOUPI_DATABASE_USER,
                "passwd": YOUPI_DATABASE_PASSWD,
                "db": YOUPI_DATABASE_NAME
            }

        db = Database(**args)
        db.model.generate_mapping(create_tables=True)
        binder.bind(Database, to=db, scope=singleton)

        for service in SERVICES:
            binder.bind(service, scope=singleton)

        for integration in INTEGRATIONS:
            binder.bind(integration, scope=singleton)

        binder.bind(ProcessPayments, scope=singleton)


    injector = Injector(modules=[configure])

    process_payments = injector.get(ProcessPayments)
    process_payments.process_payments()
