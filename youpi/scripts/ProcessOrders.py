import os
import sys
import datetime
sys.path.append(os.path.dirname(os.getcwd()))
import requests
import base64
import logging
from youpi.models.entities import Database
from youpi.services import SERVICES, SaleService
from pony.orm import db_session
from youpi.integration import INTEGRATIONS
from injector import singleton, Injector, inject
from chronopost.Order import Order
from chronopost.ChronopostService import ChronopostService
from drivefx_python_sdk.invoice import InvoiceService, Product, Invoice, Costumer
from chronopost_python_sdk import ChronopostService as ChronopostServiceSDK, Delivery
from youpi.commons.exceptions import FileManagerUploadError

YOUPI_DATABASE_TYPE = os.environ.get("YOUPI_DATABASE_TYPE", "sqlite")
YOUPI_DATABASE_URL = os.environ.get("YOUPI_DATABASE_URL", os.path.join("..", "..", "data", "database.sqlite"))
YOUPI_DATABASE_HOST = os.environ.get("YOUPI_DATABASE_HOST", "127.0.0.1")
YOUPI_DATABASE_USER = os.environ.get("YOUPI_DATABASE_USER", "root")
YOUPI_DATABASE_PASSWD = os.environ.get("YOUPI_DATABASE_PASSWD", "Pocosi12!")
YOUPI_DATABASE_NAME = os.environ.get("YOUPI_DATABASE_NAME", "youpi")
YOUPI_LOCALHOST_PORT = int(os.environ.get("YOUPI_LOCALHOST_PORT", "5000"))
YOUPI_DEBUG_FLAG = bool(os.environ.get("YOUPI_DEBUG_FLAG", "True"))
FILE_MANAGER_URL = os.environ.get("FILE_MANAGER_URL", "https://api.filemanager.betacode.tech/v1")
DRIVEFX_BACKEND_URL = os.environ.get("DRIVEFX_BACKEND_URL", "https://sis08.drivefx.net/794D265E")
DRIVEFX_APP_ID = os.environ.get("DRIVEFX_APP_ID", "ABF5746AB5")
DRIVEFX_USER_CODE = os.environ.get('DRIVEFX_USER_CODE', "tfcmarques98@gmail.com")
DRIVEFX_PASSWORD = os.environ.get("DRIVEFX_PASSWORD", 'passwordyoupi')
# DRIVEFX_BACKEND_URL = os.environ.get("DRIVEFX_BACKEND_URL", "https://sis08.drivefx.net/a1b6c79d")
# DRIVEFX_APP_ID = os.environ.get("DRIVEFX_APP_ID", "ABF5746AB5")
# DRIVEFX_USER_CODE = os.environ.get('DRIVEFX_USER_CODE', "mac.mendao@gmail.com")
# DRIVEFX_PASSWORD = os.environ.get("DRIVEFX_PASSWORD", 'passwordyoupi')
IMAGE_FILTERS_URL = os.environ.get("IMAGE_FILTERS_URL", "https://api.imgfiltering.betacode.tech/v1")
EMAIL_SERVICE_URL = os.environ.get("EMAIL_SERVICE_URL", "http://api.emailhandler.betacode.tech/v1")
FACTORY_EMAIL = os.environ.get("FACTORY_EMAIL", "laboratorio@youpii.pt")
RAFIK_EMAIL = os.environ.get("RAFIK_EMAIL", "rafik@w-group.pt")
INFO_BETACODE_EMAIL = os.environ.get("INFO_BETACODE_EMAIL", "info@betacode.tech")
PEDRO_PALRAO_EMAIL = os.environ.get("PEDRO_PALRAO_EMAIL", "pedropalrao@makeadream.pt")

ORIGIN_NAME = os.environ.get("ORIGIN_NAME", "Youpi! Your Best Feeling")
ORIGIN_ADDRESS = os.environ.get("ORIGIN_ADDRESS", 'Estrada da Outurela n° 118 Edifício Holanda, Bloco 1')
ORIGIN_ZIP_CODE = os.environ.get('ORIGIN_ZIP_CODE', '2794-084')
ORIGIN_CITY = os.environ.get('ORIGIN_CITY', 'Carnaxide')
ORIGIN_MAIL = os.environ.get('ORIGIN_MAIL', 'laboratorio@youpii.pt')


class ProcessOrders:
    @inject
    def __init__(self, sales_service: SaleService, db: Database,
                 chronopost_service: ChronopostService, invoice_service: InvoiceService,
                 chronopost_service_sdk: ChronopostServiceSDK):
        self.chronopost_service = chronopost_service
        self.sales_service = sales_service
        self.db = db
        self.invoice_service = invoice_service
        self.chronopost_service_sdk = chronopost_service_sdk

    def send_client_email(self, costumer, sale, products, invoice):
        # costumer_info = self.map_costumer_to_email(costumer)
        # delivery_info = self.map_sale_to_email(sale=sale, invoice=invoice)

        addresses = sale['extra']['addresses']

        client_email = costumer.email
        client_name = costumer.first_name + ' ' + costumer.last_name
        client_phone = addresses['shipping_phone_number']

        # delivery_name = costumer.first_name + ' ' + costumer.last_name
        delivery_name = addresses['shipping_name']
        delivery_address = f'{addresses["shipping_address"]}, {addresses["shipping_city"]}, {addresses["shipping_zip_code"]}'
        # billing_name = costumer.first_name + ' ' + costumer.last_name
        billing_name = addresses['billing_name']
        # billing_nif = addresses['billing_nif']
        billing_address = f'{addresses["billing_address"]}, {addresses["billing_city"]}, {addresses["billing_zip_code"]}'
        sale_uid = sale['uuid']
        sale_date = sale['timestamp']
        sale_total = sale['total']
        sale_receipt = invoice
        payment_method = sale['payment_type']

        if 'billing_nif' in addresses:
            billing_nif = addresses['billing_nif']
        else:
            billing_nif = ''

        input = {
            "client_email": client_email,
            "client_name": client_name,
            "client_phone": client_phone,
            "delivery_name": delivery_name,
            "delivery_address": delivery_address,
            "billing_name": billing_name,
            "billing_nif": billing_nif,
            "billing_address": billing_address,
            "sale_uid": sale_uid,
            "sale_date": sale_date,
            "sale_total": sale_total,
            "sale_receipt": sale_receipt,
            "payment_method": payment_method,
            "products": products
        }

        url = f"{EMAIL_SERVICE_URL}/email/purchase_receipt"
        response = requests.post(url, json=input)

        if response.status_code != requests.codes.ok:
            raise FileManagerUploadError(response.raise_for_status())

        response_json = response.json()

        return response_json

    def send_factory_email(self, costumer, sale, products, invoice, chronopost):
        addresses = sale['extra']['addresses']

        emails = [FACTORY_EMAIL, RAFIK_EMAIL, INFO_BETACODE_EMAIL]
        # emails = [INFO_BETACODE_EMAIL]

        for e in emails:
            input = {
                "email_dest": e,
                "client_name": costumer.first_name + ' ' + costumer.last_name,
                # "client_nif": addresses['billing_nif'],
                "client_email": costumer.email,
                "client_address": f'{addresses["billing_address"]}, {addresses["billing_city"]}, {addresses["billing_zip_code"]}',
                "client_phone": addresses['shipping_phone_number'],
                "sale_uid": sale['uuid'],
                "sale_total": sale['total'],
                "sale_date": sale['timestamp'],
                "payment_method": sale["payment_type"],
                "sale_receipt": invoice,
                "chronopost_file": chronopost,
                "products": products
            }

            if 'billing_nif' in addresses:
                billing_nif = addresses['billing_nif']
            else:
                billing_nif = ''

            input['client_nif'] = billing_nif

            payment_info = sale['extra']['payment_info']

            if sale['payment_type'] == 'multibanco':
                input['payment_mb_reference'] = payment_info['reference']
                input['payment_mb_entity'] = payment_info['entity']
            if sale['payment_type'] == 'payshop':
                input['payment_ps_reference'] = payment_info['reference']
                input['payment_ps_limit_date'] = payment_info['paymentDeadline']

            url = f"{EMAIL_SERVICE_URL}/email/purchase_info"
            response = requests.post(url, json=input)

            if response.status_code != requests.codes.ok:
                raise FileManagerUploadError(response.raise_for_status())

            response_json = response.json()

            logging.info(f'Email sent to {e}')

    # Converts a string to a csv file and encodes it in base 64
    def to_csv_file(self, txt):
        file = txt.encode('utf-8')

        url = f"{FILE_MANAGER_URL}/files/upload"

        response = requests.post(url, files={"file": ('chronopost.csv', file)})
        response_json = response.json()

        if response.status_code != requests.codes.ok:
            raise FileManagerUploadError(response.raise_for_status())

        return FILE_MANAGER_URL + response_json['url']

    # 1. Calls get_chronopost_file from chronopost sdk and assigns the returned value
    # to a variable
    # 2. Creates a file from the variable
    # 3. Sends the file to our file manager (Post through the api) that return a 'url'
    # 4. Add the url to the sale.extra['chronopost_url']
    # 5. Return sale
    def generate_chronopost_file(self, order):
        today = datetime.datetime.now()

        dest_name = order.dest_name
        dest_zip_code = order.dest_pcode
        dest_city = order.dest_pcodelocal
        dest_address = order.dest_address
        nr_items = order.ship_label_of
        shipping_date =today

        delivery = Delivery(OrigemMoradaNome=ORIGIN_NAME, OrigemMorada=ORIGIN_ADDRESS, OrigemMoradaCodigoPostal=ORIGIN_ZIP_CODE,
                            OrigemMoradaLocalidade=ORIGIN_CITY, OrigemEmail=ORIGIN_MAIL, DestinoMoradaNome=dest_name,
                            DestinoMoradaCodigoPostal=dest_zip_code, DestinoMoradaLocalidade=dest_city,
                            DestinoMorada=dest_address, NumeroVolumes=nr_items, DataExpedicao=shipping_date)

        response = self.chronopost_service_sdk.dispatch(delivery)

        # shipping_guide = response['CorpoResposta']

        return response

        # txt = self.chronopost_service.get_chronopost_file([order])

        # return self.to_csv_file(txt)

        # encoded_file = self.to_csv_file(txt)

    def get_filtered_photo(self, simulation_data):
        url = f"{IMAGE_FILTERS_URL}/filter"
        f_manager_url = f"{FILE_MANAGER_URL}/files/upload"

        filters = {
            "img_file": simulation_data['original_photo'],
            "brightness": simulation_data['brightness'],
            "contrast": simulation_data['contrast'],
            "saturation": simulation_data['saturation'],
            "temperature": simulation_data['temperature'],
            "auto_resize": 0
        }

        response = requests.put(url, data=filters)

        if response.status_code != requests.codes.ok:
            raise FileManagerUploadError(response.raise_for_status())

        filtered_image = base64.b64encode(response.content)
        img_index = bytes("data:image/jpeg;base64,", encoding="utf-8")
        filtered_b64 = str(img_index + filtered_image)[2:-1]

        file_manager_response = requests.post(f_manager_url, files={"file": ('simulation.png', response.content)})

        response_json = file_manager_response.json()

        if file_manager_response.status_code != requests.codes.ok:
            raise FileManagerUploadError(file_manager_response.raise_for_status())

        # simulation = {
        #     "gama": simulation_data['gama'],
        #     "pattern": simulation_data['pattern'],
        #     "size": simulation_data['size'],
        #     "have_white_margin": simulation_data['have_white_margin'],
        #     "url": response_json['url']
        # }
        #
        # return simulation
        filtered_photo = FILE_MANAGER_URL + response_json['url']
        return filtered_photo

    # Transforms a sale in an order
    @db_session
    def process_sale(self, sale):
        logging.info(f'Processing sale {sale["uuid"]}')
        costumer_id = sale['costumer']
        costumer = self.db.model.Costumer[costumer_id]
        extra = sale['extra']
        addresses = extra['addresses']
        dest_name = addresses['shipping_name']
        dest_address = addresses['shipping_address']
        dest_pcode = addresses['shipping_zip_code']
        dest_pcodelocal = addresses['shipping_city']
        dest_mobile = costumer.contact
        dest_email = costumer.email
        ship_label_of = len(sale['cart']['products'])

        products = sale['cart']['products']
        simulations = []

        # for p in products:
        #     if p['extra']['simulation_data'] is not None:
        #         processed = self.process_simulation(p['extra']['simulation_data'])
        #         simulations.append(processed)

        order = Order(dest_name=dest_name, dest_address=dest_address, dest_pcode=dest_pcode,
                      dest_pcodelocal=dest_pcodelocal, dest_mobile_telef=dest_mobile,
                      dest_email=dest_email, ship_label_of=ship_label_of)

        chronopost_file = self.generate_chronopost_file(order)

        if 'CorpoResposta' not in chronopost_file:
            raise Exception("Error generating chronopost file")

        shipping_guide = chronopost_file['CorpoResposta']
        logging.info('Chronopost file generated')
        invoice = self.create_invoice(costumer, sale)
        # invoice = 'https://png.pngtree.com/element_our/md/20180515/md_5afb099d307d3.jpg'
        logging.info('Invoice generated')

        if "payment_info" in extra:
            payment_info = {**extra["payment_info"]}
            payment_info["invoice_url"] = invoice
            self.sales_service.update_payment_info(sale["uuid"], payment_info)

        products_info = self.process_products(products=products)
        logging.info('Products processed')

        self.send_client_email(costumer=costumer, sale=sale, products=products_info,
                               invoice=invoice)
        logging.info('Client email sent')

        self.send_factory_email(costumer=costumer, sale=sale, products=products_info,
                                invoice=invoice, chronopost=shipping_guide)

        result = {
            "simulations": simulations,
            "chronopost": chronopost_file,
            "invoice": invoice,
            "products": products_info
        }

        self.sales_service.change_sale_status_to_processed(sale['uuid'])

        return result

    def process(self):
        logging.basicConfig(level=logging.DEBUG)
        logging.info('Process orders begin...')
        sales = self.sales_service.get_sales_to_process()

        logging.info(f'Processing {len(sales)} orders...')

        for sale in sales:
            try:
                self.process_sale(sale)
            except Exception as e:
                logging.error(str(e))

        logging.info('Process orders end...')

    def create_invoice(self, costumer, sale):
        invoice_products = []

        for product in sale['cart']['products']:
            invoice_products.append(self.create_product(product))

        invoice_costumer = self.create_costumer(sale, costumer)

        logging.info(str(invoice_costumer.__dict__))
        logging.info(str([c.__dict__ for c in invoice_products]))

        invoice = Invoice(costumer=invoice_costumer, products=invoice_products)

        return self.generate_invoice(invoice)

    def generate_invoice(self, invoice):
        invoice = self.invoice_service.create_invoice(invoice=invoice)

        return invoice['pdf']

    def create_product(self, product):
        reference = product['reference']
        designation = product['name']
        unit_price = product['price_base']
        discount_price = product['price_promo']

        discount1 = 0

        if unit_price != 0 and discount_price is not None:
            discount1 = round(100 - ((discount_price / unit_price) * 100))

        invoice_product = Product(reference=reference, designation=designation,
                                  unit_price=unit_price, discount1=discount1)

        return invoice_product

    def create_costumer(self, sale, costumer):
        extra = sale['extra']
        addresses = extra['addresses']
        costumer_extra = costumer.extra

        name = addresses['billing_name']
        email = costumer.email
        nif = addresses['billing_nif']
        address = addresses['billing_address']
        zip_code = addresses['billing_zip_code']
        city = addresses['billing_city']
        #number = "1000" + str(costumer.id)
        number = 1

        if addresses['billing_nif'] is not '':
            number = int(addresses['billing_nif'])

        invoice_costumer = Costumer(number=number, name=name, email=email, tax_number=nif, address=address,
                                    postal_code=zip_code, city=city)

        return invoice_costumer

    # def map_costumer_to_email(self, costumer):
    #     client_email = costumer.email
    #     client_name = costumer.first_name + ' ' + costumer.last_name
    #     client_phone = costumer.contact
    #
    #     costumer = {
    #         "client_email": client_email,
    #         "client_name": client_name,
    #         "client_phone": client_phone
    #     }
    #
    #     return costumer

    def process_products(self, products):
        products_info = []

        for p in products:
            product_name = p['name']
            product_price = p['price_base']
            product_ref = p['reference']
            product_info = {
                "product_name": product_name,
                "product_price": product_price,
                "product_ref": product_ref

            }
            if p['extra'] is not None:
                simulation_data = p['extra']['simulation_data'] if 'simulation_data' in p['extra'] else None
                if simulation_data is not None:
                    frame_size = simulation_data['size'][0]
                    frame_type = simulation_data['gama']
                    white_margin = simulation_data['have_white_margin']
                    picture_size = simulation_data['size'][1]

                    print_picture = self.get_filtered_photo(simulation_data=simulation_data)
                    product_info['frame_size'] = frame_size
                    product_info['frame_type'] = frame_type
                    product_info['white_margin'] = white_margin
                    product_info['picture_size'] = picture_size
                    product_info['print_picture'] = print_picture

            products_info.append(product_info)

        return products_info

    # def map_sale_to_email(self, sale, invoice):
    #     addresses = sale['extra']['addresses']
    #
    #     # delivery_name = costumer.first_name + ' ' + costumer.last_name
    #     delivery_name = addresses['shipping_name']
    #     delivery_address = addresses['shipping_address']
    #     # billing_name = costumer.first_name + ' ' + costumer.last_name
    #     billing_name = addresses['billing_name']
    #     # billing_nif = costumer.extra['nif']
    #     billing_nif = addresses['billing_nif']
    #     billing_address = addresses['billing_address']
    #     sale_uid = sale['uuid']
    #     sale_date = sale['timestamp']
    #     sale_total = sale['total']
    #     sale_receipt = invoice
    #     payment_method = sale['payment_type']
    #
    #     delivery = {
    #         "delivery_name": delivery_name,
    #         "delivery_address": delivery_address,
    #         "billing_name": billing_name,
    #         "billing_nif": billing_nif,
    #         "billing_address": billing_address,
    #         "sale_uid": sale_uid,
    #         "sale_date": sale_date,
    #         "sale_total": sale_total,
    #         "sale_receipt": sale_receipt,
    #         "payment_method": payment_method
    #     }
    #
    #     return delivery


if __name__ == "__main__":
    def configure(binder):
        args = {}

        if YOUPI_DATABASE_TYPE in (None, "sqlite"):
            args = {
                "provider": "sqlite",
                "filename": YOUPI_DATABASE_URL,
                "create_db": True
            }
        elif YOUPI_DATABASE_TYPE == "mysql":
            args = {
                "provider": "mysql",
                "host": YOUPI_DATABASE_HOST,
                "user": YOUPI_DATABASE_USER,
                "passwd": YOUPI_DATABASE_PASSWD,
                "db": YOUPI_DATABASE_NAME
            }

        db = Database(**args)
        db.model.generate_mapping(create_tables=True)
        binder.bind(Database, to=db, scope=singleton)

        invoice_service = InvoiceService(backend_url=DRIVEFX_BACKEND_URL, app_id=DRIVEFX_APP_ID,
                                         user_code=DRIVEFX_USER_CODE, password=DRIVEFX_PASSWORD)

        binder.bind(InvoiceService, to=invoice_service, scope=singleton)

        chronopost_service_sdk = ChronopostServiceSDK(username='024961', password='X024961X', email='laboratorio@youpii.pt')

        binder.bind(ChronopostServiceSDK, to=chronopost_service_sdk, scope=singleton)

        for service in SERVICES:
            binder.bind(service, scope=singleton)

        for integration in INTEGRATIONS:
            binder.bind(integration, scope=singleton)

        binder.bind(ProcessOrders, scope=singleton)


    injector = Injector(modules=[configure])

    process_orders = injector.get(ProcessOrders)

    process_orders.process()
