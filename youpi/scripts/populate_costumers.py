import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
import random
from pony.orm import db_session
from pony.orm.core import ObjectNotFound
from flask_injector import inject, singleton
from injector import Injector
from youpi.services import SERVICES
from youpi.models.entities import Database
from youpi.services.CostumerService import CostumerService
from youpi.services.StoreService import StoreService


class PopulateCostumers:
    @inject
    def __init__(self, db: Database, costumer_service: CostumerService, store_service: StoreService):
        self.db = db
        self.costumer_service = costumer_service
        self.store_service = store_service

    def populate_costumers(self):
        self.create_default_store()

        for i in range(5):
            self.create_costumer(i)

    @db_session
    def create_default_store(self):
        try:
            self.db.model.Store[1]

        except ObjectNotFound:
            store = {
                "name": "Default Store",
                "description": "Default Description"
            }
            self.store_service.add_update(**store)

    def create_costumer(self, i):
        costumer = {
            "email": "costumer" + repr(i),
            "first_name": "first_name" + repr(i),
            "last_name": "last_name" + repr(i),
            "store": 1,
            "extra": {},
            "password": "password" + repr(i),
            "local": "somewhere" + repr(i),
            "contact": str(random.randrange(900000000, 1000000000)),
            "volume": random.randrange(0, 10000),
            "total_spent": round(random.uniform(0.0, 2000.0), 2)
        }
        return self.costumer_service.add_update(**costumer)


if __name__ == "__main__":
    def configure(binder):
        args = {
            "provider": "sqlite",
            "filename": os.path.join("..", "..", "data", "database.sqlite"),
            "create_db": True
        }

        # args = {
        #    "provider": "mysql",
        #    "host": "127.0.0.1",
        #    "user": "root",
        #    "passwd": "Pocosi12!",
        #    "db": "dev_youpi"
        # }

        db = Database(**args)
        db.model.generate_mapping(create_tables=True)
        binder.bind(Database, to=db, scope=singleton)

        for service in SERVICES:
            binder.bind(service, scope=singleton)


    injector = Injector(modules=[configure])

    db = injector.get(Database)
    populator = injector.get(PopulateCostumers)

    populator.populate_costumers()