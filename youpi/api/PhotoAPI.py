import werkzeug.datastructures
from flask_injector import inject
from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_raw_jwt
from youpi.services.PhotoService import PhotoService
from pony.orm.core import ObjectNotFound
from youpi.commons.exceptions import NoPhotosFound, InvalidVariant, AlreadyAssignedPhoto, UnassignedPhoto,\
    FileManagerUploadError, FileManagerDeleteError
from youpi.commons.exception_handler import ExceptionHandler


class UploadPhoto(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/product/<int:id>/photo/upload"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("file", type=werkzeug.datastructures.FileStorage, location="files", required=True)
        self.reqparse.add_argument("Authorization", location="headers")

    def post(self, id):
        args = self.reqparse.parse_args()

        try:
            file = args.file
            token = args.Authorization
            file_content = file.read()

            return self.photo_service.upload_photo_to_product(file_content, file.filename, file.content_type, token, id)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()
        except FileManagerUploadError as e:
            return ExceptionHandler.file_manager_upload_error(e)


class AddVariantPhoto(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/photo/<int:photo>/variant/<int:variant>/add"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

    def put(self, photo, variant):
        try:
            self.photo_service.add_variant_photo(photo, variant)
            return {"message": "Successfully added photo from variant."}
        except AlreadyAssignedPhoto:
            return ExceptionHandler.already_assigned_photo()
        except InvalidVariant:
            return ExceptionHandler.invalid_variant()
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class RemoveVariantPhoto(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/photo/<int:photo>/variant/remove"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

    def put(self, photo):
        try:
            self.photo_service.remove_variant_photo(photo)
            return {"message": "Successfully removed photo from variant."}
        except UnassignedPhoto:
            return ExceptionHandler.unassigned_photo()
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class Photo(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/photo/<int:id>"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("Authorization", location="headers")

    def get(self, id):
        try:
            return self.photo_service.get_photo_by_id(id)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def delete(self, id):
        args = self.reqparse.parse_args()

        try:
            token = args.Authorization
            return self.photo_service.delete_photo(id, token)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()
        except FileManagerDeleteError as e:
            return ExceptionHandler.file_manager_delete_error(e)


class ProductPhotos(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/product/<int:id>/photos"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

    def get(self, id):
        try:
            return self.photo_service.get_photos_by_product(id)
        except NoPhotosFound:
            return ExceptionHandler.no_photos_found()


class VariantPhotos(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/variant/<int:id>/photos"

    @inject
    def __init__(self, photo_service: PhotoService):
        self.photo_service = photo_service

    def get(self, id):
        try:
            return self.photo_service.get_photos_by_variant(id)
        except NoPhotosFound:
            return ExceptionHandler.no_photos_found()
