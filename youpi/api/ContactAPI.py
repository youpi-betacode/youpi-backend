import pandas
import numpy
import werkzeug.datastructures
from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import jwt_required
from youpi.services.ContactService import ContactService
from pony.orm.core import ObjectNotFound, TransactionIntegrityError
from youpi.commons.exception_handler import ExceptionHandler
from youpi.commons.exceptions import RepeatedContacts, EmailRequired


class Contacts(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/contacts"

    @inject
    def __init__(self, contact_service: ContactService):
        self.contact_service = contact_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("desc", type=bool)

    def get(self):
        args = self.reqparse.parse_args()

        try:
            return self.contact_service.get_all_contacts(args.page, args.page_size, args.order_by, args.desc)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class ImportContacts(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/contacts/import"

    @inject
    def __init__(self, contact_service: ContactService):
        self.contact_service = contact_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("file", type=werkzeug.datastructures.FileStorage, location="files")

    def post(self):
        args = self.reqparse.parse_args()

        try:
            file_content = pandas.read_csv(args.file.stream, delimiter=",", encoding="latin1")
            contacts = file_content.replace([numpy.nan], [None]).to_dict('records')
            self.contact_service.add_contacts(contacts)

            return {"message": "Successfully imported contacts."}

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()

        except RepeatedContacts as e:
            return ExceptionHandler.repeated_contacts(e.index)

        except EmailRequired as e:
            return ExceptionHandler.email_required(e.index)

        except IndexError:
            return ExceptionHandler.delimiter_error()


class ExportContacts(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/contacts/export"

    @inject
    def __init__(self, contact_service: ContactService):
        self.contact_service = contact_service

    def get(self):
        try:
            response = self.contact_service.get_all_contacts(None, None, None, None)

            contacts_csv = "First_Name,Last_Name,Email,Phone_Number,Address,Birth_Date,Segment,Company," \
                           "Business_Area,Local\n"

            for contact in response["contacts"]:
                del contact["id"]
                contacts_csv += f"{contact['first_name']},{contact['last_name']},{contact['email']}," \
                                f"{contact['phone_number']},{contact['address']},{contact['birth_date']}," \
                                f"{contact['segment']},{contact['company']},{contact['business_area']}," \
                                f"{contact['local']}\n"

            return contacts_csv.replace('None', '')

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()
