from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import get_jwt_identity, jwt_required, jwt_optional
from pony.orm.core import ObjectNotFound
from youpi.commons.exceptions import MissingVariantFromCart, LoadedCartIsEmpty
from youpi.commons.exception_handler import ExceptionHandler
from youpi.services.ShoppingCartService import ShoppingCartService


class ShoppingCart(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/cart"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def get(self):
        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])

            if cart is None:
                cart = self.cart_service.create_shopping_cart(current_user["id"])

            return cart

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class UpdateShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/update"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("local_cart", type=dict, default={})

    def put(self):
        args = self.reqparse.parse_args()

        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])

            products_list = args.local_cart["products"]

            if not products_list:
                raise LoadedCartIsEmpty

            for key, value in products_list.items():
                product_ref = key
                product_info = value
                cart = self.cart_service.add_n_products_to_cart(cart["id"], product_ref, product_info["amount"])

            if "extra" in args.local_cart:
                if "simulations" in args.local_cart["extra"]:
                    simulations_info = args.local_cart["extra"]["simulations"]

                    for simulation in simulations_info:
                        cart = self.cart_service.add_simulation_info_to_cart(cart["id"], simulation)

            return cart

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except LoadedCartIsEmpty:
            return ExceptionHandler.loaded_cart_empty()


class AddSimulationsToShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/simulations"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("local_cart", type=dict, default={})

    def put(self):
        args = self.reqparse.parse_args()

        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])

            if "extra" in args.local_cart:
                if "simulations" in args.local_cart["extra"]:
                    simulations_info = args.local_cart["extra"]["simulations"]

                    for simulation in simulations_info:
                        cart = self.cart_service.add_simulation_info_to_cart(cart["id"], simulation)
                else:
                    return {"message": "No simulations found in the shopping cart."}
            return cart

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class AddVariantToShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/add/<string:product>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def put(self, product):
        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])
            return self.cart_service.add_product_to_cart(cart["id"], product)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class AddNVariantsToShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/add/<string:product>/<int:amount>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def put(self, product, amount):
        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])
            return self.cart_service.add_n_products_to_cart(cart["id"], product, amount)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class RemoveVariantFromShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/remove/<string:product>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def put(self, product):
        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])
            return self.cart_service.remove_product_from_cart(cart["id"], product)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except MissingVariantFromCart:
            return ExceptionHandler.missing_variant_from_cart()


class RemoveNVariantsFromShoppingCart(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/cart/remove/<string:product>/<int:amount>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def put(self, product, amount):
        try:
            current_user = get_jwt_identity()
            cart = self.cart_service.get_cart_by_costumer(current_user["id"])
            return self.cart_service.remove_n_products_from_cart(cart["id"], product, amount)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except MissingVariantFromCart:
            return ExceptionHandler.missing_variant_from_cart()
