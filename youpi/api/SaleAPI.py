from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import jwt_required, get_jwt_identity
from youpi.services.SaleService import SaleService
from pony.orm.core import ExprEvalError
from youpi.commons.exception_handler import ExceptionHandler
from youpi.commons.exceptions import ApplicationException


class Sale(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/costumer/sale"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("payment", type=str, required=True)
        self.reqparse.add_argument("shopping_cart", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})

    def post(self):
        args = self.reqparse.parse_args()
        current_costumer = get_jwt_identity()

        try:
            sale = self.sale_service.create_sale(costumer=current_costumer["id"], payment=args.payment,
                                                 shopping_cart_uuid=args.shopping_cart, extra=args.extra)
            products = sale['cart']['products']

            products_info = self.sale_service.process_products(products=products)
            self.sale_service.send_purchase_summary(sale=sale, costumer=current_costumer, products=products_info)

            return sale
        except ApplicationException as e:
            return {"message": e.msg}, 400


class SaleByUUID(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/sale/<string:uuid>"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    def get(self, uuid):
        try:
            return self.sale_service.get_sale_by_uuid(uuid)
        except ApplicationException as e:
            return {"message": e.msg}, 400


class CostumerSales(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/costumer/sales"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)

    def post(self):
        args = self.reqparse.parse_args()

        try:
            costumer = get_jwt_identity()
            costumer_id = costumer['id']

            return self.sale_service.get_sales_by_costumer(costumer_id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesByStore(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/sales"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("descendent", type=bool)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.sale_service.get_sales_by_store(id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()
