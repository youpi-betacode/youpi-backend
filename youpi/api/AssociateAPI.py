from flask_restful import Resource, reqparse
from flask_injector import inject
from youpi.services.AssociateService import AssociateService
from flask_jwt_extended import jwt_required
from youpi.commons.exception_handler import ExceptionHandler
from youpi.commons.exceptions import InvalidAssociation
from pony.orm.core import ObjectNotFound, ExprEvalError, TransactionIntegrityError


class Associate(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/associate"

    @inject
    def __init__(self, associate_service: AssociateService):
        self.associate_service = associate_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("password", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("associate_type", type=str, required=True)
        self.reqparse.add_argument("entity_name", type=str, required=True)
        self.reqparse.add_argument("contract_exp", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})
        self.reqparse.add_argument("username", type=str, default=None)
        self.reqparse.add_argument("local", type=str, default=None)
        self.reqparse.add_argument("contact", type=str, default=None)
        self.reqparse.add_argument("volume", type=int, default=None)
        self.reqparse.add_argument("total_spent", type=float, default=None)
        self.reqparse.add_argument("sales_in_store", type=float, default=None)
        self.reqparse.add_argument("sales_online", type=float, default=None)

    def post(self, id):
        args = self.reqparse.parse_args()

        try:
            associate = self.associate_service.add_update(args.email, args.first_name, args.last_name, id,
                                                          args.associate_type, args.entity_name, args.contract_exp,
                                                          args.extra, args.password, args.username, args.local,
                                                          args.contact, args.volume, args.total_spent,
                                                          args.sales_in_store, args.sales_online)
            return associate
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except InvalidAssociation:
            return ExceptionHandler.invalid_association()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()


class AssociateByID(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/associate/<int:id>"

    @inject
    def __init__(self, associate_service: AssociateService):
        self.associate_service = associate_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("associate_type", type=str, required=True)
        self.reqparse.add_argument("entity_name", type=str, required=True)
        self.reqparse.add_argument("contract_exp", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})
        self.reqparse.add_argument("username", type=str, default=None)
        self.reqparse.add_argument("local", type=str, default=None)
        self.reqparse.add_argument("contact", type=int, default=None)
        self.reqparse.add_argument("volume", type=int, default=None)
        self.reqparse.add_argument("total_spent", type=float, default=None)
        self.reqparse.add_argument("sales_in_store", type=float, default=None)
        self.reqparse.add_argument("sales_online", type=float, default=None)

    def get(self, id):
        try:
            return self.associate_service.get_associate_by_id(id)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def delete(self, id):
        try:
            self.associate_service.delete_associate(id)
            return {"message": "Successfully deleted product."}, 200
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def put(self, id):
        args = self.reqparse.parse_args()

        try:
            associate = self.associate_service.get_associate_by_id(id)
            updated_associate = self.associate_service.add_update(args.email, args.first_name, args.last_name,
                                                                  associate["store"]["id"], args.associate_type,
                                                                  args.entity_name, args.contract_exp, args.extra,
                                                                  None, args.username, args.local,
                                                                  args.contact, args.volume, args.total_spent,
                                                                  args.sales_in_store, args.sales_online,
                                                                  associate["id"])
            return updated_associate
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()


class Partners(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/partners"

    @inject
    def __init__(self, associate_service: AssociateService):
        self.associate_service = associate_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("descendent", type=bool)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.associate_service.get_partners_by_store(id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class Sponsors(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/sponsors"

    @inject
    def __init__(self, associate_service: AssociateService):
        self.associate_service = associate_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("descendent", type=bool)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.associate_service.get_sponsors_by_store(id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()
