from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import jwt_required
from youpi.services.ProductService import ProductService
from pony.orm.core import ObjectNotFound, ExprEvalError
from youpi.commons.exception_handler import ExceptionHandler


class Product(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/product"

    @inject
    def __init__(self, product_service: ProductService):
        self.product_service = product_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("name", type=str, required=True)
        self.reqparse.add_argument("description", type=str, required=True)
        self.reqparse.add_argument("category", type=str, required=True)
        self.reqparse.add_argument("supplier", type=int, required=True)

    def post(self, id):
        args = self.reqparse.parse_args()

        try:
            product = self.product_service.add_update(name=args.name, description=args.description,
                                                      category=args.category, store_id=id)
            return product

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class ProductByID(Resource):
    DECORATORS = []
    ENDPOINT = "/store/<int:store_id>/product/<string:reference>"

    @inject
    def __init__(self, product_service: ProductService):
        self.product_service = product_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("name", type=str, required=True)
        self.reqparse.add_argument("description", type=str, required=False)
        self.reqparse.add_argument("category", type=str, required=False)
        self.reqparse.add_argument("supplier", type=str, required=False)

    def get(self, store_id, reference):
        try:
            return self.product_service.get_product_by_id(reference)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def delete(self, store_id, reference):
        try:
            self.product_service.delete_product(reference)
            return {"message": "Successfully deleted product."}, 200
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def put(self, store_id, reference):
        args = self.reqparse.parse_args()

        try:
            updated_product = self.product_service.add_update(name=args.name, description=args.description,
                                                              category=args.category, store_id=store_id,
                                                              reference=reference)
            return updated_product
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class ProductsByStore(Resource):
    DECORATORS = []
    ENDPOINT = "/store/<int:store_id>/product/list"

    @inject
    def __init__(self, product_service: ProductService):
        self.product_service = product_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("category", type=str)
        self.reqparse.add_argument("sub_category", type=str)
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("desc", type=bool)

    def post(self, store_id):
        args = self.reqparse.parse_args()

        try:
            return self.product_service.get_product_parents_by_store(store=store_id, category=args.category,
                                                              sub_category=args.sub_category, page=args.page,
                                                              page_size=args.page_size, order_by=args.order_by,
                                                              descendent=args.desc)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class RelatedProducts(Resource):
    DECORATORS = []
    ENDPOINT = "/store/<int:store_id>/product/<string:reference>/related"

    @inject
    def __init__(self, product_service: ProductService):
        self.product_service = product_service

    def get(self, store_id, reference):

        try:
            return self.product_service.get_related_products_parents_by_store(store=store_id, reference=reference)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class ExportProducts(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/product/export"

    @inject
    def __init__(self, product_service: ProductService):
        self.product_service = product_service

    def get(self, id):
        try:
            response = self.product_service.get_products_by_store(id, None, None, None, None, None, None)

            products_csv = "Product_ID,Name,Description,Category,Supplier_ID,Last_Sale\n"

            for product in response["products"]:
                products_csv += f"{product['id']},{product['name']},{product['description']},{product['category']}," \
                                f"{product['supplier']['id']},{product['last_sale']}\n"

            return products_csv.replace('None', '')

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()
