from flask_restful import Resource, reqparse
from flask_injector import inject
from youpi.services.CostumerService import CostumerService
from flask_jwt_extended import get_jwt_identity, jwt_required
from youpi.commons.exceptions import EmailFailed
from youpi.commons.exception_handler import ExceptionHandler
from pony.orm.core import ObjectNotFound, ExprEvalError, TransactionIntegrityError


class SignupCostumer(Resource):
    DECORATORS = []
    ENDPOINT = "/store/<int:id>/signup/costumer"

    @inject
    def __init__(self, costumer_service: CostumerService):
        self.costumer_service = costumer_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("password", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})
        self.reqparse.add_argument("local", type=str, required=True)
        self.reqparse.add_argument("contact", type=str)

    def post(self, id):
        args = self.reqparse.parse_args()

        try:
            costumer = self.costumer_service.add_update(args.email, args.first_name, args.last_name, id, args.extra,
                                                        args.password, args.local, args.contact)
            return costumer
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()


class Costumer(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/costumer"

    @inject
    def __init__(self, costumer_service: CostumerService):
        self.costumer_service = costumer_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})
        self.reqparse.add_argument("local", type=str, required=True)
        self.reqparse.add_argument("contact", type=str)

    def put(self):
        args = self.reqparse.parse_args()

        try:
            current_costumer = get_jwt_identity()
            updated_costumer = self.costumer_service.add_update(args.email, args.first_name, args.last_name,
                                                                current_costumer["store"], args.extra, None, args.local,
                                                                args.contact, None, None, current_costumer["id"])
            return updated_costumer
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()

        except EmailFailed:
            return ExceptionHandler.email_failed()


class CostumerByStore(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:store_id>/costumers"

    @inject
    def __init__(self, costumer_service: CostumerService):
        self.costumer_service = costumer_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("desc", type=bool)

    def get(self, store_id):
        args = self.reqparse.parse_args()

        try:
            return self.costumer_service.get_costumers_by_store(store_id, args.page, args.page_size,
                                                                args.order_by, args.desc)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()
