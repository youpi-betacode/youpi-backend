from flask_restful import Resource, reqparse
from flask_injector import inject
from youpi.services.UserService import UserService
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
from youpi.commons.exception_handler import ExceptionHandler
from youpi.commons.exceptions import NonMatchingPassword, InvalidRole, MissingPassword, EmailFailed
from pony.orm.core import ObjectNotFound, TransactionIntegrityError


class SignupUser(Resource):
    DECORATORS = []
    ENDPOINT = "/store/<int:id>/signup/user"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("password", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("role", type=int, required=True)
        self.reqparse.add_argument("extra", type=dict, default={})

    def post(self, id):
        args = self.reqparse.parse_args()

        try:
            user = self.user_service.add_update(args.email, args.first_name, args.last_name,
                                                args.role, id, args.extra, args.password)
            return user
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except InvalidRole:
            return ExceptionHandler.invalid_role()

        except MissingPassword:
            return ExceptionHandler.missing_password()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()


class UserPassword(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/user/reset_password"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("password", required=True)

    def put(self):
        args = self.reqparse.parse_args()

        try:
            current_user = get_jwt_identity()
            updated_user = self.user_service.reset_user_password(current_user["id"], args.password)
            return updated_user

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except EmailFailed:
            return ExceptionHandler.email_failed()


class RecoverPassword(Resource):
    DECORATORS = []
    ENDPOINT = "/recover_password/<string:email>"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def post(self,email):

        try:
            updated_user = self.user_service.recover_password(email)
            return updated_user

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class Login(Resource):
    DECORATORS = []
    ENDPOINT = "/login"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("identifier", type=str, required=True)
        self.reqparse.add_argument("password", required=True)

    def post(self):
        args = self.reqparse.parse_args()

        try:
            user = self.user_service.authenticate(args.identifier, args.password)
            access_token = create_access_token(identity=user, expires_delta=False)
            user["token"] = access_token
            return user

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except NonMatchingPassword:
            return ExceptionHandler.invalid_credentials()


class Logout(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/logout"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def post(self):
        try:
            user = get_jwt_identity()
            user["token"] = None
            return {"message": "Successfully logged out."}, 200

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class CheckEmail(Resource):
    DECORATORS = []
    ENDPOINT = "/check-email/<string:email>"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def get(self, email):
        try:
            user = self.user_service.get_user_by_email(email)
            if user is not None:
                return {"message": True}, 200
            else:
                return {"message": False}, 200
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class User(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/user"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("email", type=str, required=True)
        self.reqparse.add_argument("password", type=str, required=True)
        self.reqparse.add_argument("first_name", type=str, required=True)
        self.reqparse.add_argument("last_name", type=str, required=True)
        self.reqparse.add_argument("role", type=int, required=True)
        self.reqparse.add_argument("extra", type=str, default={})

    def get(self):
        try:
            current_user = get_jwt_identity()
            return self.user_service.get_user_by_id(current_user["id"])

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def put(self):
        args = self.reqparse.parse_args()
        try:
            current_user = get_jwt_identity()
            updated_user = self.user_service.add_update(args.email, args.first_name, args.last_name, args.role,
                                                        current_user["store"], args.extra, None, current_user["id"])
            return updated_user

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

        except InvalidRole:
            return ExceptionHandler.invalid_role()

        except TransactionIntegrityError:
            return ExceptionHandler.transaction_integrity_error()

    def delete(self):
        try:
            current_user = get_jwt_identity()
            self.user_service.delete_user(current_user["id"])
            return {"message": "Successfully deleted user."}, 200

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class Users(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/users"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def get(self, id):
        try:
            users = self.user_service.get_users_by_store(id)
            return users

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class EmailConfirmation(Resource):
    DECORATORS = []
    ENDPOINT = "/email/<string:uuid>"

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def get(self, uuid):
        try:
            return self.user_service.confirm_user_email(uuid)

        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()
