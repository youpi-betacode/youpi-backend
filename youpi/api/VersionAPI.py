from flask_restful import Resource
from flask_injector import inject
from youpi.services.VersionService import VersionService


class Version(Resource):

    DECORATORS = []
    ENDPOINT = "/version"

    @inject
    def __init__(self, version_service: VersionService):
        self.version_service = version_service

    def get(self):
        return self.version_service.get_actual_version()
