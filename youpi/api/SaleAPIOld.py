from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import jwt_required, get_jwt_identity
from youpi.services.SaleService import SaleService
from pony.orm.core import ObjectNotFound, ExprEvalError
from youpi.commons.exception_handler import ExceptionHandler
from youpi.commons.exceptions import EmptyShoppingCart, PaymentRequestFailed, ApplicationException


class Sale(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/costumer/sale"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("payment", type=str, required=True)
        self.reqparse.add_argument("shopping_cart", type=str, required=True)
        self.reqparse.add_argument("extra", type=dict)

    def post(self):
        args = self.reqparse.parse_args()
        current_costumer = get_jwt_identity()

        try:
            return self.sale_service.add_sale(costumer=current_costumer["id"], payment=args.payment,
                                              shopping_cart_uuid=args.shopping_cart, extra=args.extra)
        except ApplicationException as e:
            return {"message": e.msg}, 400


class SaleByID(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/sale/<string:uuid>"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    def get(self, uuid):
        try:
            return self.sale_service.get_sale_by_uuid(uuid)
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()

    def delete(self, id):
        try:
            self.sale_service.delete_sale(id)
            return {"message": "Successfully deleted product."}, 200
        except ObjectNotFound:
            return ExceptionHandler.obj_not_found()


class SalesByCostumer(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/costumer/<int:id>/sales"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("descendent", type=bool)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.sale_service.get_sales_by_costumer(id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesByStore(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/sales"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("page", type=int)
        self.reqparse.add_argument("page_size", type=int)
        self.reqparse.add_argument("order_by", type=str)
        self.reqparse.add_argument("descendent", type=bool)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.sale_service.get_sales_by_store(id, **args)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesByChannel(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/graphs/channels"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    def get(self, id):
        try:
            return self.sale_service.get_channel_sales(id)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesByCategory(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/graphs/categories"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("timespan", type=int, required=True)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.sale_service.get_categories_sales(id, args.timespan)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesBySponsor(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/graphs/sponsors"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    def get(self, id):
        try:
            return self.sale_service.get_sponsor_sales(id)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class SalesByProduct(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/store/<int:id>/graphs/products"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("timespan", type=int, required=True)

    def get(self, id):
        args = self.reqparse.parse_args()

        try:
            return self.sale_service.get_products_sales(id, args.timespan)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()


class ProductSales(Resource):
    DECORATORS = [jwt_required]
    ENDPOINT = "/product/<int:id>/sales"

    @inject
    def __init__(self, sale_service: SaleService):
        self.sale_service = sale_service

    def get(self, id):
        try:
            return self.sale_service.get_sales_by_product(id)
        except ExprEvalError:
            return ExceptionHandler.expr_eval_error()
