from flask_restful import Resource, reqparse
from flask_injector import inject
from flask_jwt_extended import get_jwt_identity, jwt_required, jwt_optional
from pony.orm.core import ObjectNotFound
from youpi.commons.exceptions import MissingVariantFromCart, LoadedCartIsEmpty, ApplicationException
from youpi.commons.exception_handler import ExceptionHandler
from youpi.services.ShoppingCartService import ShoppingCartService
import json


class CreateShoppingCart(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/store/<int:store_id>/cart"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def get(self, store_id):
        try:
            current_user = get_jwt_identity()
            if current_user is None:
                return self.cart_service.create_shopping_cart(store_id=store_id)
            else:
                return self.cart_service.get_costumer_cart(costumer_id=current_user["id"])

        except ApplicationException as e:
            return {"message": e.msg}, 400


class GetShoppingCart(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/store/<int:store_id>/cart/<cart_uuid>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

    def get(self, store_id, cart_uuid):
        try:
            current_user = get_jwt_identity()
            if current_user is not None:
                return self.cart_service.get_shopping_cart(uuid=cart_uuid, costumer_id=current_user["id"])
            else:
                return self.cart_service.get_shopping_cart(uuid=cart_uuid)

        except ApplicationException as e:
            return {"message": e.msg}, 400


class AddUpdateShoppingCartProduct(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/store/<int:store_id>/cart/<cart_uuid>/<product_reference>"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

        self.post_reqparse = reqparse.RequestParser()
        self.post_reqparse.add_argument("amount", type=int, required=True)
        self.post_reqparse.add_argument("extra", type=dict, required=False, default=None)

    def post(self, store_id, cart_uuid, product_reference):
        try:
            args = self.post_reqparse.parse_args()

            return self.cart_service.add_update_quantity_product_cart(uuid=cart_uuid, reference=product_reference,
                                                                      amount=args.amount, extra=args.extra)

        except ApplicationException as e:
            return {"message": e.msg}, 400

    def delete(self, store_id, cart_uuid, product_reference):
        try:
            return self.cart_service.delete_product_cart(uuid=cart_uuid, reference=product_reference)
        except ApplicationException as e:
            return {"message": e.msg}, 400


class IncrementShoppingCartProduct(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/store/<int:store_id>/cart/<cart_uuid>/<product_reference>/increment"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

        self.post_reqparse = reqparse.RequestParser()
        self.post_reqparse.add_argument("extra", type=dict, required=False, default={})

    def post(self, store_id, cart_uuid, product_reference):
        try:
            args = self.post_reqparse.parse_args()

            return self.cart_service.increment_product_cart(uuid=cart_uuid, reference=product_reference, extra=args.extra)

        except ApplicationException as e:
            return {"message": e.msg}, 400


class DecrementShoppingCartProduct(Resource):
    DECORATORS = [jwt_optional]
    ENDPOINT = "/store/<int:store_id>/cart/<cart_uuid>/<product_reference>/decrement"

    @inject
    def __init__(self, cart_service: ShoppingCartService):
        self.cart_service = cart_service

        self.post_reqparse = reqparse.RequestParser()
        self.post_reqparse.add_argument("extra", type=dict, required=False, default=None)

    def post(self, store_id, cart_uuid, product_reference):
        try:
            args = self.post_reqparse.parse_args()

            return self.cart_service.decrement_product_cart(uuid=cart_uuid, reference=product_reference, extra=args.extra)

        except ApplicationException as e:
            return {"message": e.msg}, 400
