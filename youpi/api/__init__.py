from .VersionAPI import Version
from .UserAPI import SignupUser, Login, Logout, User, Users, UserPassword, EmailConfirmation, \
    CheckEmail, RecoverPassword
from .CostumerAPI import SignupCostumer, Costumer, CostumerByStore
from .ProductAPI import Product, ProductByID, ProductsByStore, ExportProducts, RelatedProducts
from .ContactAPI import Contacts, ImportContacts, ExportContacts
from .AssociateAPI import Associate, AssociateByID, Partners, Sponsors
from .SaleAPI import Sale, SaleByUUID, CostumerSales, SalesByStore
from .PhotoAPI import UploadPhoto, AddVariantPhoto, RemoveVariantPhoto, Photo, ProductPhotos, VariantPhotos
from .ShoppingCartAPI import CreateShoppingCart, GetShoppingCart, AddUpdateShoppingCartProduct, IncrementShoppingCartProduct, DecrementShoppingCartProduct

API_HANDLERS = [
    Version,
    SignupUser,
    Login,
    Logout,
    User,
    Users,
    UserPassword,
    Product,
    ProductByID,
    ProductsByStore,
    ExportProducts,
    Contacts,
    ImportContacts,
    ExportContacts,
    Associate,
    AssociateByID,
    Partners,
    Sponsors,
    Sale,
    SaleByUUID,
    UploadPhoto,
    AddVariantPhoto,
    RemoveVariantPhoto,
    Photo,
    ProductPhotos,
    VariantPhotos,
    SignupCostumer,
    Costumer,
    CostumerByStore,
    EmailConfirmation,
    CheckEmail,
    RecoverPassword,
    CreateShoppingCart,
    GetShoppingCart,
    AddUpdateShoppingCartProduct,
    IncrementShoppingCartProduct,
    DecrementShoppingCartProduct,
    RelatedProducts,
    CostumerSales,
    SalesByStore
]
