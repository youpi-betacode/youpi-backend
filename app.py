#!/usr/bin/env python
import os
import sys
import xmltodict
import logging
from flask import Flask, request
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_restful import Api
from flask_injector import singleton, FlaskInjector
from youpi.api import API_HANDLERS
from youpi.models.entities import Database
from youpi.services import SERVICES, StoreService, UserService, SaleService
from youpi.integration.DriveFX import DriveFXService
from youpi.integration import INTEGRATIONS
import pony
sys.path.append(os.path.dirname(os.getcwd()))

YOUPI_DATABASE_TYPE = os.environ.get("YOUPI_DATABASE_TYPE", "sqlite")
YOUPI_DATABASE_URL = os.environ.get("YOUPI_DATABASE_URL", os.path.join("..", "..", "data", "database.sqlite"))
YOUPI_DATABASE_HOST = os.environ.get("YOUPI_DATABASE_HOST", "127.0.0.1")
YOUPI_DATABASE_USER = os.environ.get("YOUPI_DATABASE_USER", "root")
YOUPI_DATABASE_PASSWD = os.environ.get("YOUPI_DATABASE_PASSWD", "Pocosi12!")
YOUPI_DATABASE_NAME = os.environ.get("YOUPI_DATABASE_NAME", "youpi")
YOUPI_LOCALHOST_PORT = int(os.environ.get("YOUPI_LOCALHOST_PORT", "5000"))
YOUPI_DEBUG_FLAG = bool(os.environ.get("YOUPI_DEBUG_FLAG", "True"))

print(f"YOUPI_DATABASE_TYPE: {YOUPI_DATABASE_TYPE}")
print(f"YOUPI_DATABASE_HOST: {YOUPI_DATABASE_HOST}")
print(f"YOUPI_DATABASE_USER: {YOUPI_DATABASE_USER}")
print(f"YOUPI_DATABASE_PASSWD: {YOUPI_DATABASE_PASSWD}")
print(f"YOUPI_DATABASE_NAME: {YOUPI_DATABASE_NAME}")
print(f"YOUPI_LOCALHOST_PORT: {YOUPI_LOCALHOST_PORT}")
print(f"YOUPI_DEBUG_FLAG: {YOUPI_DEBUG_FLAG}")

API_PREFIX = "/v1"

app = Flask(__name__)
CORS(app)
api = Api(app=app, prefix=API_PREFIX)


for handler in API_HANDLERS:
    handler.decorators = handler.DECORATORS
    api.add_resource(handler, handler.ENDPOINT)


def configure(binder):
    args = {}

    if YOUPI_DATABASE_TYPE in (None, "sqlite"):
        args = {
            "provider": "sqlite",
            "filename": YOUPI_DATABASE_URL,
            "create_db": True
        }
    elif YOUPI_DATABASE_TYPE == "mysql":
        args = {
            "provider": "mysql",
            "host": YOUPI_DATABASE_HOST,
            "user": YOUPI_DATABASE_USER,
            "passwd": YOUPI_DATABASE_PASSWD,
            "db": YOUPI_DATABASE_NAME
        }

    db = Database(**args)
    db.model.generate_mapping(create_tables=True)
    binder.bind(Database, to=db, scope=singleton)

    for service in SERVICES:
        binder.bind(service, scope=singleton)

    for integration in INTEGRATIONS:
        binder.bind(integration, scope=singleton)


injector = FlaskInjector(app=app, modules=[configure])

store_service = injector.injector.get(StoreService)
drivefx_service = injector.injector.get(DriveFXService)

# Add default store
store_service.add_default_store()
# sync products with drivefx on startup
drivefx_service.sync_products()

app.config["JWT_SECRET_KEY"] = "pocosi12!"
jwt = JWTManager(app)


@jwt.user_loader_callback_loader
def user_loader_callback(identity):

    user_service = injector.injector.get(UserService)
    try:
        user = user_service.get_user_by_id(identity["id"])
        return user
    except pony.orm.core.ObjectNotFound:
        return None


# Credit Card Payment - HiPay Callback Endpoint
@app.route('/v1/hipay/callback/<sale_uuid>', methods=['POST'])
def callback(sale_uuid):
    if request.method == 'POST':
        logging.basicConfig(level=logging.INFO)
        sale_service = injector.injector.get(SaleService)

        logging.info(f"Received callback for sale with UUID: {sale_uuid}")
        xml_form = request.form['xml']
        content_dict = xmltodict.parse(xml_form)
        result = content_dict['mapi']['result']
        status = result['status']

        logging.info(f"Checking sale {sale_uuid} status...")
        if status == "ok":
            logging.info(f"Successful sale. Updating sale status to paid.")
            sale_service.change_sale_status_to_paid(sale_uuid)
        else:
            logging.info(f"Unsuccessful sale. Updating sale status to failed.")
            sale_service.change_sale_status_to_failed(sale_uuid)

        sale_service.update_payment_info(sale_uuid, result)
        logging.info(f"Finished processing sale {sale_uuid}")
        return {"message": "Finished callback."}, 200

    return {"message": "Bad request."}, 400


if __name__ == "__main__":
    app.run(debug=YOUPI_DEBUG_FLAG, port=YOUPI_LOCALHOST_PORT)
