# youpi-backend

## Setup:

1- Verify Python 3 and Pipenv are installed.

2- Run `pipenv install` inside the project folder.

## Run Project:

1- Run `pipenv shell`

2- Run `python app.py`.